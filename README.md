2019 GameDev (OOP)
=====

# Sprint Board

Note: plz update this sprint board every time you push

## Task Board:
### Backlog
- XML Map read
- enemy
- attack
- refine the ships in myGame and collider initialization in destroyer class

### Doing
- sprite update

### Done
- select ship (single)
- mouse_selector test
- multiple ships select
- collider

## Known issues:
- sinking one ships will cause exception (at collider controller)

## --> sprint 5/3 to 5/10 :( <--
- UI update
- attack system

#### Update Notes:
- 31/3/8: Some how I think I've lost the file somewhere, since I couldn't recover it, I'll replace it with this new one. :(
- TODO: OnClick event for every GameObject in game.

# Index
- Road to Pacific
- Makefile and the test stuffs
- VS Game Test Field
- GameObject Design
	* Notations
	* GameComponent

# Road to Pacific
Nothing to mention (at least for now),
here's a parrot to keep this space filled.
![image](https://cultofthepartyparrot.com/parrots/parrot.gif)

# Makefile and the test stuffs
- test: all the test cases are in this directory.
- makefile: Only builds the tests, not able to deal with the game framework :(

# VS Game Test Field
This directory is for projects of testing new functions.
Now include:
	* Map Test

# GameObject Design
GameObject is an abstract class contains different GameComponents.
A new game object in game should implement the GameObject interface.

Example:
```cpp
class Ship: public GameObject {
		// Some more codes
};
```

## Notations
### ScreenCoordinate and GameObjectPosition
Two coordinate system are used in this architecture.
The term "Coordinate" is used for screen coodinate, while
"Position" is applied for the abstract in-game coordinate system and is stored in `GameComponent` `Transform` in every `GameObject`.

The basic conversion of the two are showed as follow:
```
ScreenCoordinate = mapPosition + gamePosition
``` 

## GameComponent
GameComponent is the interface of all different components of a gameobject.
List gameComponents:
	* Transform: Stores the gameObject's position, default GameComponent for every GameObject
	* GameImage: Stores the gameObject's CMovingBitmap and show it(need to pass map coordinate)
GameComponents needed to be implemented:
	* Collider:
	* RigidBody: GameObject's velocity and acceleration, physic status

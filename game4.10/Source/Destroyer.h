#ifndef DESTROYER_H
#define DESTROYER_H

#include <string>
#include "Ship.h"

class Destroyer : public Ship{
public:
	/*Destroyer is a basic ship object*/
	Destroyer(std::string shipName, unsigned int hitPoint);
	Destroyer(std::string shipName, unsigned int hitPoint, int positionX, int positionY, bool isFriend = true);
};

#endif

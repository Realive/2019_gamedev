#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include <list>
#include "ship.h"
#include "borderland.h"

class GameManager {
public:
	/*Managing game's behaviour*/
    GameManager(std::list<Ship *> * ship, std::list<GameObject *> * gameObject);
    
	// to clean useless object
	void update();

	// clean shell which has arrived it's destination
	void cleanUsedShell();

	// to see if is winning
	bool isWin(Borderland *border);
	
	// to see if is losing
	bool isLose(Borderland *border);
private:
	std::list<GameObject *> * gameObjects;
    std::list<Ship *> * ships;
};

#endif

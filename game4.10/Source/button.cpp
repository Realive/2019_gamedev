#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
	#include "gamelib.h"
#endif
#include <string>

#include "button.h"
#include "vector2D.h"
#include "ui.h"
#include "text.h"

Button::Button(std::string buttonText, Vector2D<int> buttonCordinate) {
	this->buttonCordinate = buttonCordinate;
	Vector2D<int> textCordinate = Vector2D<int>(buttonCordinate.getX() + 23, buttonCordinate.getY() + 1);
	this->buttonText = new Text(buttonText, textCordinate);
	this->buttonText->setColor(0, 0, 0);
	this->buttonText->setBkColor(255, 255, 255);
	buttonImage.LoadBitmap("Bitmaps/button.bmp");
	buttonImage.SetTopLeft(buttonCordinate.getX(), buttonCordinate.getY());
}

void Button::setButtonText(std::string buttonText) {
	this->buttonText->setText(buttonText);
}

void Button::setButtonCordinate(Vector2D<int> buttonCordinate) {
	this->buttonCordinate = buttonCordinate;
}

void Button::show() {
	buttonImage.ShowBitmap();
	buttonText->show();
}
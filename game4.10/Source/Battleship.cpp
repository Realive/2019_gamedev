#include "StdAfx.h"
#ifdef _MSC_VER
    #include <ddraw.h>
#endif

#include "Battleship.h"

Battleship::Battleship(std::string shipName, unsigned int hitPoint) : Ship(shipName, hitPoint) {}
Battleship::Battleship(std::string shipName, unsigned int hitPoint, int positionX, int positionY, bool isFriend) : Ship(shipName, hitPoint, positionX, positionY, isFriend) {
	std::vector<std::string> images;
	for (size_t i = 0; i < 8; ++i) {
		std::string file_name = "Bitmaps/bb/bb_s_" + std::to_string(i) + ".bmp";
		images.push_back(file_name);
	}
	addImage(images, 8);
	addComponent(new Collider(getComponent<Transform>(), getComponent<GameImage>()));
}
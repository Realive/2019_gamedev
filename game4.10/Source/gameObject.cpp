#include "StdAfx.h"
#ifdef _MSC_VER
    #include <ddraw.h>
#endif

#include "gameObject.h"
#include "game_image.h"
#include "collider.h"
#include "convert_extension.h"

GameObject::GameObject(std::string objectName) {
    gameComponents.push_back(new Transform(0, 0));
    name = objectName;
}

GameObject::GameObject(std::string objectName, int initPositionX, int initPositionY) {
    gameComponents.push_back(new Transform(initPositionX, initPositionY));
    name = objectName;
}

std::string GameObject::getName () const {
    return name;
}

void GameObject::addComponent (GameComponent * component) {
    gameComponents.push_back(component);
}

void GameObject::addImage (std::vector<std::string> imagePaths, size_t imageNum) {
	gameComponents.push_back(new GameImage(imagePaths, imageNum));
}

void GameObject::showImage(int mapCoordinateX, int mapCoordinateY) {
	try {
		Transform * transform = getComponent<Transform>();
		GameImage * gameImage = getComponent<GameImage>();
		gameImage->showImage(mapCoordinateX + transform->getPosition().getX(),
							 mapCoordinateY + transform->getPosition().getY());
	}
	catch (std::string e) {
		/*change the exception to std::exception*/
	}
}

template<class ComponentType>
ComponentType * GameObject::getComponent() {
	for (auto it = gameComponents.begin(); it != gameComponents.end(); ++it) {
		if (typeid(ComponentType) == typeid(**it))
			return (ComponentType*)(*it);
	}
	throw (std::string)"Component with specified type could not be found.";
}

void GameObject::setPosition(const Vector2D<int> & newPosition) {
	Transform * transform = getComponent<Transform>();
	transform->setPosition(newPosition);
}

void GameObject::setDirection(int degree) {
	Transform * transform = getComponent<Transform>();
	transform->setDirection(degree);
}

void GameObject::setDirection(const Vector2D<double> & newDirection) {
	Transform * transform = getComponent<Transform>();
	transform->setDirection(newDirection);
}

bool GameObject::isClicked(const Vector2D<int> & mousePosition) {
    Collider * collider = getComponent<Collider>();
    return collider->isClicked(mousePosition);
}

bool GameObject::isInSelectArea(const Vector2D<int> & mouseTopLeft, const Vector2D<int> & mouseBottomRight) {
    Collider * collider = getComponent<Collider>();
    return collider->isInSelectArea(mouseTopLeft, mouseBottomRight);
}

Vector2D<int> GameObject::getPosition() {
	Transform * transform = getComponent<Transform>();
	return transform->getPosition();
}

Vector2D<double> GameObject::getDirection() {
	Transform * transform = getComponent<Transform>();
	return transform->getDirection();
}

Collider GameObject::getCollider() {
	return *getComponent<Collider>();
}

GameObject::~GameObject() {
	for (auto it = gameComponents.begin(); it != gameComponents.end(); ++it)
		delete (*it);
}

template Transform * GameObject::getComponent<Transform> ();
template GameImage * GameObject::getComponent<GameImage> ();

#ifndef BUTTON_H
#define BUTTON_H

#include <string>
#include "ui.h"
#include "text.h"
#include "vector2D.h"

class Button : public UI {
public:
	/*We actually did not use this UI object, it is useable, but lack of behaviour*/
	Button(std::string buttonText, Vector2D<int> buttonCordinate);

	// set button's text
	void setButtonText(std::string buttonText);

	// set button's position
	void setButtonCordinate(Vector2D<int> buttonCordinate);
	
	// show button
	void show();
private:
	game_framework::CMovingBitmap buttonImage;
	Text* buttonText;
	Vector2D<int> buttonCordinate;
};

#endif // !BUTTON_H

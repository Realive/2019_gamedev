#include "StdAfx.h"
#ifdef _MSC_VER
    #include <ddraw.h>
#endif
#include "Destroyer.h"
#include "collider.h"

Destroyer::Destroyer(std::string shipName, unsigned int hitPoint) : Ship(shipName, hitPoint) {}
Destroyer::Destroyer(std::string shipName, unsigned int hitPoint, int positionX, int positionY, bool isFriend)
	: Ship(shipName, hitPoint, positionX, positionY, isFriend) {

	/* Add images */
	std::vector<std::string> images;
	for (size_t i = 0; i < 8; ++i) {
		std::string friendly = "";

		if (isFriend == true)
			friendly = "_f";

		std::string file_name = "Bitmaps/ship_" + std::to_string(i) + friendly + ".bmp";
		images.push_back(file_name);
	}
	addImage(images, 8);

	addComponent(new Collider(getComponent<Transform>(), getComponent<GameImage>()));
}

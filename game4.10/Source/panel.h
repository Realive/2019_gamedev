#ifndef PANEL_H
#define PANEL_H

#include <ddraw.h>
#include <string>
#include "ui.h"
#include "Ship.h"
#include "text.h"
#include "vector2D.h"
#include "borderland.h"

class Panel : public UI {
public:
	/*Showing the status of game*/
	Panel();
	~Panel();

	// show panel
	void show();

	// get ship selected
	void getSelectedShips(std::list<Ship *> selectedShips);
	
	// set borderland's text
	void setBorderland(std::vector<Borderland *>* borderlands);
private:
	game_framework::CMovingBitmap panelImage;
	std::list<UI*> shipTextList;
	std::list<UI*> borderTextList;
	Vector2D<int> panelCordinate = Vector2D<int> (0, 520);
};

#endif
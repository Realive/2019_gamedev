#include "StdAfx.h"
#include "score.h"

Score::Score() { }

Score::~Score() {
	delete instance;
}

Score* Score::instance = nullptr;

Score* Score::getInstance() {
	if (instance == nullptr)
		instance = new Score();
	return instance;
}

void Score::addScore(int i) {
	this->score += i;
}

int Score::getScore() {
	return this->score;
}

void Score::setWinState(bool state) {
	winState = state;
}

bool Score::getWinState() {
	return winState;
}

void Score::addString(std::string text) {
	texts.push_back(text);
}

std::list<std::string> Score::getTexts() {
	return texts;
}

void Score::addLifeTime() {
	lifeTime++;
}

int Score::getLifeTime() const {
	return lifeTime;
}
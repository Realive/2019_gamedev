#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
#endif

#include "mouse_selector.h"

MouseSelector::MouseSelector(std::list<Ship *> * ships) {
	this->ships = ships;
}

MouseSelector::~MouseSelector() { 

}

void MouseSelector::update(const Vector2D<int> & mapCoord) {
	mapCoordinate = mapCoord;
}

void MouseSelector::setStatusText(Text * shipStatusText) {
	this->shipStatusText = shipStatusText;
}

void MouseSelector::onMouseLDown(int x, int y) {
	mouseLClickPoint = Vector2D<int>(x, y);
	selectedShips.clear();
	Vector2D<int> mousePosition = converter.convertScreenToMap(Vector2D<int>(x, y), mapCoordinate);
	for (auto it = ships->begin(); it != ships->end(); ++it) {
		if ((*it)->isClicked(mousePosition))
			selectedShips.push_back(*it);
	}
}

void MouseSelector::onMouseLUp(int x, int y) {
	if (!(mouseLClickPoint == Vector2D<int>(x, y))) {
		selectedShips.clear();
		Vector2D<int> mousePosition = converter.convertScreenToMap(Vector2D<int>(x, y), mapCoordinate);
		for (auto it = ships->begin(); it != ships->end(); ++it) {
			if ((*it)->isInSelectArea(mouseLClickPoint, mousePosition))
				selectedShips.push_back(*it);
		}
	}
}

std::list<Ship *> MouseSelector::getSelectedShips() {
	return selectedShips;
}

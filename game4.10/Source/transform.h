#ifndef TRANSFORM_H
#define TRANSFORM_H
/*
 * Definition of class Transform
 * Created by Chikuma(Neuenmuller), 03/21/2019
 */

#include "gameComponent.h"
#include "vector2D.h"

class Transform : public GameComponent {
public:
	/*Physical behaviour of objects*/
    Transform () {}
    Transform (unsigned int x, unsigned int y);
    
	// to know what type object is
	std::string getType();

	// to get object's position
    Vector2D<int> getPosition();

	// to get object's heading point
	Vector2D<double> getDirection();

	// to set object's position
    void setPosition(const Vector2D<int> & position);

	// to set object's heading point
	void setDirection(double degree);	// The speed of the gameObject should be defined at the derived class of gameObject
	
	// to set heading point with vector
	void setDirection(Vector2D<double> newDirection);

private:
    Vector2D<int> position;
	Vector2D<double> direction;  /* The default direciton degree = 90 */
};

#endif

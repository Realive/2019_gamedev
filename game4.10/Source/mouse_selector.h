#ifndef MOUSE_SELECTOR_H
#define MOUSE_SELECTOR_H

#include "ship.h"
#include "convert_extension.h"
#include "text.h"

class MouseSelector {
public:
	/*To see if mouse selected anything*/
	MouseSelector(std::list<Ship *> * ships);
	~MouseSelector();

	// update selecting data
	void update(const Vector2D<int> & mapCoord);
	
	// set anything selected in text
	void setStatusText(Text * shipStatusText);
	
	// behaviour while mouse click
	void onMouseLDown(int x, int y);

	// behaviour while mouse leave
	void onMouseLUp(int x, int y);

	// get all ship selected
	std::list<Ship *> getSelectedShips();
private:
	Vector2D<int> mapCoordinate;
	Vector2D<int> mouseLClickPoint;
	std::list<Ship *> * ships = nullptr;
	std::list<Ship *> selectedShips;
	MapScreenConvert converter;
	Text * shipStatusText;
};

#endif

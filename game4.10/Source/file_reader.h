#ifndef FILE_READER_H
#define FILE_READER_H
#include <string>
#include <list>

std::list<std::list<std::string>> fileReading(std::string path);
void removeSpace(std::string & str);
std::list<std::string> splitElementByComma(std::string);


#endif // !FILE_READER_H

#ifndef SCORE_H
#define SCORE_H

#include <list>
#include <string>

class Score {
public:
	// singleton
	static Score* getInstance();
	
	// add score if couquer border
	void addScore(int i);
	
	// get final score
	int getScore();

	// set if is win
	void setWinState(bool state);
	
	// get state of winning or losing
	bool getWinState();

	// add string
	void addString(std::string text);
	
	// add survive time
	void addLifeTime();

	// get survive time
	int getLifeTime() const;

	// get saved string
	std::list<std::string> getTexts();
private :
	static Score* instance;
	/*To count all Score during game*/
	Score();
	~Score();
	int score = 0;
	int lifeTime = 0;
	bool winState;
	std::list<std::string> texts;
};

#endif // !SCORE_H
#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
#endif

#include <math.h>
#include "gun_shell.h"

GunShell::GunShell(int damage, int init_x, int init_y, int dest_x, int dest_y)
	:GameObject("shell", init_x, init_y), destination(dest_x, dest_y), damage(damage) {

	std::vector<std::string> images;
	for (int i = 0; i < 8; i++) {
		std::string path = "Bitmaps/gunshell/gunshell_" + std::to_string(i) + ".bmp";
		images.push_back(path);
	}
	addImage(images, 8);
	setDirection((destination - getPosition()).getUnitVector());
	updateAppearance();
}

void GunShell::update() {
	if (isArrive()) {
		ttlCountDown();
		speed = 0;
		addComponent(new Collider(getComponent<Transform>(), getComponent<GameImage>()));
	}

	setPosition(getPosition() + getDirection() * speed);
	setDirection((destination - getPosition()).getUnitVector());
}

void GunShell::ttlCountDown() {
	ttl--;
	if (ttl <= 0)
		hasCausedDamage();
}

void GunShell::setSpeed(int speed) {
	this->speed = speed;
}

void GunShell::hasCausedDamage() {
	damage = 0;
}

bool GunShell::isArrive() {
	if ((destination - getPosition()).getLength() < speed || speed == 0)
		return true;
	return false;
}

bool GunShell::hasCollider() {
	try {
		Collider * collider = getComponent<Collider>();
	} catch (std::string e) {
		return false;
	}
	return true;
}

int GunShell::getDamage() const {
	return damage;
}

void GunShell::updateAppearance() {
	int imageCode = 0;

	if (getDirection().getY() > sin(2 * 3.141 / 16 * 3))
		imageCode = 4;
	else if (getDirection().getY() < sin(2 * 3.141 / 16 * 11))
		imageCode = 0;
	else if (getDirection().getY() < sin(2 * 3.141 / 16 * 3) && getDirection().getY() > sin(2 * 3.141 / 16))
		(getDirection().getX() > 0) ? imageCode = 3 : imageCode = 5;
	else if (getDirection().getY() < sin(2 * 3.141 / 16 * 9) && getDirection().getY() > sin(2 * 3.141 / 16 * 11))
		(getDirection().getX() > 0) ? imageCode = 1 : imageCode = 7;
	else if (getDirection().getY() < sin(2 * 3.141 / 16) && getDirection().getY() > sin(2 * 3.141 / 16 * 9))
		(getDirection().getX() > 0) ? imageCode = 2 : imageCode = 6;

	getComponent<GameImage>()->setImage(imageCode);
}

#ifndef MOVE_OPERATION_H
#define MOVE_OPERATION_H

#include "ship_operation.h"
#include "mouse_selector.h"

class MoveOperation : public ShipOperation {
public:
	/*Set way point or directly*/
    MoveOperation () {}
    void operate (const std::list<Ship *> & selectedShips, std::list<GameObject *> & gameObjects, const Vector2D<int> & destination, bool isShiftDown);
};

#endif

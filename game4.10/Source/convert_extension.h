#ifndef CONVERT_EXTENSION_H
#define CONVERT_EXTENSION_H

#include <string>
#include "vector2D.h"

// std::sring to cstring conversion
class Std2CString {
public:
	Std2CString () {};

	char* operator () (std::string str) {
		cPath = str;
		return &cPath[0];
	}

private:
	std::string cPath;
};

// screen coordinate to map coordinate conversion
class MapScreenConvert {
public:
	MapScreenConvert() {};

	Vector2D<int> convertScreenToMap(const Vector2D<int> & screenCoordinate, const Vector2D<int> & mapPosition) const {
		return screenCoordinate - mapPosition;
	}
};


#endif
#include <list>

#include "../file_reader.h"

TEST (FileReaderTest, RemoveSpace) {
    std::string str = "Das ist unser land!";
    removeSpace(str);
    ASSERT_EQ("Dasistunserland!", str);
}

TEST (FileReaderTest, SplitElementByComma) {
    std::string str_elements[] = {"Yosemite", "ElCaptain", "Sierra", "HighSierra", "Mojave"};
    std::list<std::string> elements = splitElementByComma("Yosemite,ElCaptain,Sierra,HighSierra,Mojave");
    int i = 0;
    for (auto it = elements.begin(); it != elements.end() && i < 5; ++it) {
        ASSERT_EQ(str_elements[i], (*it));
        ++i;
    }
}

TEST (FileReaderTest, FileReading) {
    std::list<std::list<std::string>> elements = fileReading("../test_csv/crappy_junk.csv");
    //std::string str_elements[][5] = {{"1", "2", "3", "4", "5"}, {"Yosemite", "ElCaptain", "Sierra", "HighSierra", "Mojave"}};
    ASSERT_EQ(2, elements.size());
    /*int i = 0;
    for (auto row = elements.begin(); row != elements.end()&& i < 2; ++row) {
        int j = 0;
        for (auto e = (*row).begin(); e != (*row).end() && j < 5; ++e) {
            ASSERT_EQ(str_elements[i][j], (*e));
            ++j;
        }
        ++i;
    }*/
}

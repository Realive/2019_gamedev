#include "../vector2D.h"

TEST (Vector2D, VectorInit) {
     Vector2D<int> empty_vec;     // 0, 0 as inititial value
     Vector2D<int> init_val_vec (2, 3);

     ASSERT_EQ(0, empty_vec.getX());
     ASSERT_EQ(0, empty_vec.getY());
     ASSERT_EQ(2, init_val_vec.getX());
     ASSERT_EQ(3, init_val_vec.getY());
}

TEST (Vector2D, CopyConstructor) {
    Vector2D<int> vec_1(2, 3);
    Vector2D<int> vec_2(vec_1);

    ASSERT_EQ(2, vec_2.getX());
    ASSERT_EQ(3, vec_2.getY());
}

TEST(Vector2D, AddOperator) {
    Vector2D<int> vec_l(2, 3);
    Vector2D<int> vec_r(4, 5);
    vec_l = vec_l + vec_r;

    ASSERT_EQ(6, vec_l.getX());
    ASSERT_EQ(8, vec_l.getY());
}

TEST (Vector2D, OverrideMultiplyOperator) {
    Vector2D<double> vec1(1.2, 1.3);

    ASSERT_NEAR(2.4, (vec1 * 2).getX(), 0.01);
    ASSERT_NEAR(3.9, (vec1 * 3).getY(), 0.01);
}

TEST (Vector2D, GetLength) {
    Vector2D<int> vec_1(3, 4);
    ASSERT_NEAR(5, vec_1.getLength(), 0.01);
}

TEST (Vector2D, GetUnitVector) {
	Vector2D<double> vec(3, 4);
	Vector2D<double> unit(vec.getUnitVector());
	ASSERT_NEAR(0.6, unit.getX(), 0.01);
	ASSERT_NEAR(0.8, unit.getY(), 0.01);
}

TEST (Vector2D, GetUnitVector_Int) {
	Vector2D<int> vec_int(6, 8);
	Vector2D<double> vec_int_unit(vec_int.getUnitVector());
	ASSERT_NEAR(0.6, vec_int_unit.getX(), 0.01);
	ASSERT_NEAR(0.8, vec_int_unit.getY(), 0.01);
}

TEST (Vector2D, OverrideEqualOperator) {
    Vector2D<int> vec1(2, 3);
    Vector2D<int> vec2(2, 3);
    Vector2D<int> vec3(3, 4);

    ASSERT_TRUE(vec1 == vec2);
    ASSERT_FALSE(vec2 == vec3);
    ASSERT_EQ(vec1, vec2);
}

TEST (Vector2D, OperatorPlus) {
    Vector2D<int> vec1(3, 4);
    Vector2D<double> vec2(1.3, 1.4);
    Vector2D<double> vec3(1.5, 1.6);

    ASSERT_EQ(4, (vec1 + vec2).getX());
    ASSERT_EQ(5, (vec1 + vec2).getY());
    ASSERT_EQ(5, (vec1 + vec3).getX());
    ASSERT_EQ(6, (vec1 + vec3).getY());
}

TEST (Vector2D, SubstractOperator) {
    Vector2D<int> vec_1(3, 4);
    Vector2D<int> vec_2(5, 2);
    Vector2D<int> sub = vec_1 - vec_2;

    ASSERT_EQ(-2, sub.getX());
    ASSERT_EQ(2, sub.getY());
}

TEST (Vector2D, Inverse) {
    Vector2D<int> vec(3, 4);
    Vector2D<int> vecInv = vec.inverse();

    ASSERT_EQ(-3, vecInv.getX());
    ASSERT_EQ(-4, vecInv.getY());
}

TEST (Vector2D, Distance) {
    Vector2D<int> vecA(3, 4);
    Vector2D<int> vecB(6, 8);
    ASSERT_EQ(5, distance(vecA, vecB));
}

#ifndef UT_SHIP_OPERATION_H
#define UT_SHIP_OPERATION_H

#include "../move_operation.h"
#include "../fire_operation.h"

class ShipOperationTest : public ::testing::Test {
protected:
    void SetUp () override {
        ships.push_back(new Destroyer("Fubuki", 100, 0, 0));
        ships.push_back(new Destroyer("Shigure", 90, 300, 300));
        ships.push_back(new Destroyer("Enterprise", 10, 100, 100, false));
        selector = new MouseSelector(&ships);
    }

    void TearDown () override {

    }

    std::list<Ship *> ships;
    std::list<GameObject *> gameObjects;
    MouseSelector * selector;
};

TEST_F (ShipOperationTest, OnRMouseClick) {
    MoveOperation moveOp;
    selector->onMouseLDown(10, 10);
    moveOp.operate(selector->getSelectedShips(), gameObjects, Vector2D<int> (200, 200), false);
    ships.front()->update();
    ships.front()->update();    // position of the ships wont update at the first time update is called
    ASSERT_NE(0, ships.front()->getPosition().getX());
}

TEST_F (ShipOperationTest, AttackOnRMouseClick) {
    FireOperation fireOp;
    selector->onMouseLDown(10, 10);
    fireOp.operate(selector->getSelectedShips(), gameObjects, Vector2D<int> (200, 200), false);
    ASSERT_EQ(1, gameObjects.size());
}

TEST_F (ShipOperationTest, MoveOperationOnHostile) {
    MoveOperation moveOp;
    selector->onMouseLDown(110, 110);
    moveOp.operate(selector->getSelectedShips(), gameObjects, Vector2D<int> (200, 200), false);
    for (int i = 0; i < 20; ++i)
        for (auto it = ships.begin(); it != ships.end(); ++it) {
            (*it)->update();
        }
    selector->onMouseLDown(110, 110);
    ASSERT_EQ (1, selector->getSelectedShips().size());
}

TEST_F (ShipOperationTest, FireOperationOnHostile) {
    FireOperation fireOp;
    selector->onMouseLDown(110, 110);
    fireOp.operate(selector->getSelectedShips(), gameObjects, Vector2D<int> (200, 200), false);
    for (int i = 0; i < 20; ++i)
        for (auto it = ships.begin(); it != ships.end(); ++it) {
            (*it)->update();
        }
    ASSERT_EQ (0, gameObjects.size());
}


#endif

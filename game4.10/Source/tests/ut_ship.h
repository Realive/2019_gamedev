#include <list>

#include "../gameObject.h"
#include "../transform.h"
#include "../Ship.h"
#include "../Battleship.h"
#include "../Destroyer.h"

class ShipTest : public ::testing::Test {
protected:
    void SetUp () override {
        ships[0] = new Destroyer("fubuki", 100, 0, 0);
        ships[1] = new Battleship("nagato", 1000, 0, 0);
    }

    void TearDown () override {
        for (int i = 0; i < 2; ++i)
          delete ships[i];
    }

    Ship * ships[2];
};

TEST_F (ShipTest, Composite){
    ASSERT_EQ(100, ships[0]->hitPoint());
    ASSERT_EQ(1000, ships[1]->hitPoint());
}

TEST_F (ShipTest, DD_HP) {
    Destroyer dd("Fubuki", 100);
    Destroyer dd2("murakumo", 120);
    ASSERT_EQ(100, dd.hitPoint());
    ASSERT_EQ(120, dd2.hitPoint());
}

TEST_F (ShipTest, DD_is_sunk) {
    Destroyer dd("shiratsuyu", 100);
    ASSERT_FALSE(dd.isSunk());
}

TEST_F (ShipTest, BB_HP) {
    Battleship bb("nagato", 1000);
    Battleship bb2("mutsuu", 1200);
    ASSERT_EQ(1000, bb.hitPoint());
    ASSERT_EQ(1200, bb2.hitPoint());
}

TEST_F (ShipTest, BB_is_sunk) {
    Battleship bb("shiratsuyu", 1000);
    ASSERT_FALSE(bb.isSunk());
}

TEST_F (ShipTest, GetPosition) {
    try {
        for (size_t i = 0; i < 2; i++) {
            ASSERT_EQ(0, ships[i]->getPosition().getX());
            ASSERT_EQ(0, ships[i]->getPosition().getY());
        }
    } catch (std::string e) {
        std::cout << e <<std::endl;
        FAIL();
    }
}

TEST_F (ShipTest, SetPosition) {
    Vector2D<int> position(2, 3);
    ships[0]->setPosition(position);
    ships[1]->setPosition(position);

    for (size_t i = 2; i < 2; ++i) {
        ASSERT_EQ(position, ships[i]->getPosition());
    }
}

TEST_F (ShipTest, GameObjectTransform) {
    Battleship bb("aoba", 100);
    Vector2D<int> movedPosition(5, 6);

    Transform * trans = bb.getComponent<Transform>();
    ASSERT_EQ(0, trans->getPosition().getX());
    trans->setPosition(movedPosition);
    ASSERT_EQ(5, trans->getPosition().getX());
    ASSERT_EQ(6, trans->getPosition().getY());

    delete trans;
}

TEST_F (ShipTest, ShipInitWithPosition) {
    Battleship bb("Nagato", 100, 3, 4);

    ASSERT_EQ(3, bb.getPosition().getX());
    ASSERT_EQ(4, bb.getPosition().getY());
}

TEST_F (ShipTest, SetSpeed) {
    ships[0]->setSpeed(3);
    ships[0]->update();

    ASSERT_EQ(0, ships[0]->getPosition().getX());
    ASSERT_EQ(3, ships[0]->getPosition().getY());
}

TEST_F (ShipTest, AddDestination) {
    ships[0]->addDestination(Vector2D<int> (10, 0), false);
    ships[0]->update();
    Vector2D<double> direction = ships[0]->getDirection();

    ASSERT_EQ(1, direction.getX());
    ASSERT_EQ(0, direction.getY());
}

TEST_F (ShipTest, IsArrived) {
    ships[0]->addDestination(Vector2D<int> (2, 0), false);
    ships[0]->setSpeed(1);
	ships[0]->update();
	ships[0]->update();
    ships[0]->update();

	ASSERT_EQ(2, ships[0]->getPosition().getX());
	ASSERT_EQ(0, ships[0]->getPosition().getY());
}

TEST_F (ShipTest, ComponentCollider) {
    std::vector<std::string> imgPaths;
    imgPaths.push_back("path/to/image.tiff");
    ships[0]->addImage(imgPaths, 1);
    ships[0]->addCollider();
    ASSERT_TRUE(ships[0]->isClicked(Vector2D<int>(60, 50)));
}

TEST_F (ShipTest, HitShip) {
    ships[0]->hit(100);
    ships[1]->hit(100);
    ASSERT_EQ(0, ships[0]->hitPoint());
    ASSERT_EQ(900, ships[1]->hitPoint());
    ASSERT_TRUE(ships[0]->isSunk());
}

TEST_F (ShipTest, IsFriendly) {
    ASSERT_TRUE(ships[0]->isFriendly());
    Destroyer enterprise("Enterprise", 100, 0, 0, false);
    ASSERT_FALSE(enterprise.isFriendly());
}

TEST_F (ShipTest, IsInCd) {
    ships[0]->fireShell();
    ASSERT_TRUE (ships[0]->isInCd());
    ASSERT_FALSE (ships[1]->isInCd());
    for (int i = 0; i < 50; ++i)
        ships[0]->update();
    ASSERT_FALSE (ships[0]->isInCd());
}

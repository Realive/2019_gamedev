#ifndef UT_MOUSE_SELECTOR_H
#define UT_MOUSE_SELECTOR_H

#include "../Ship.h"
#include "../mouse_selector.h"

class MouseSelectorTest : public testing::Test {
protected:
    void SetUp () override {
        ships.push_back(new Destroyer("DD-001", 100, 0, 0));
        ships.push_back(new Destroyer("DD-002", 100, 120, 110));
        ships.push_back(new Destroyer("DDG-1000", 100, 500, 600));  // live long and prosper!
    }

    void TearDown () override {
        for (auto it = ships.begin(); it != ships.end(); ++it) {
            delete *it;
        }
    }

    std::list<Ship*> ships;
};

TEST_F(MouseSelectorTest, SelectSingleShip) {
    MouseSelector ms(&ships);
    ms.onMouseLDown(10, 20);
    ms.onMouseLUp(10, 20);
    std::list<Ship*> selectedShips = ms.getSelectedShips();
    ASSERT_EQ("DD-001", selectedShips.front()->getName());
    ms.onMouseLDown(100, 200);
    ms.onMouseLUp(100, 200);
    selectedShips = ms.getSelectedShips();
    ASSERT_EQ(nullptr, selectedShips.front());
}

TEST_F(MouseSelectorTest, SelectMultipleShips) {
    MouseSelector ms(&ships);
    ms.onMouseLDown(-10, -20);
    ms.onMouseLUp(300, 400);
    std::list<Ship *> selectedShips = ms.getSelectedShips();
    ASSERT_EQ(2, selectedShips.size());
}

#endif

#include <gtest/gtest.h>
#include "ut_ship.h"
#include "ut_destroyer.h"
#include "ut_battleship.h"
#include "ut_vector_2d.h"
#include "ut_transform.h"
#include "ut_game_component.h"
#include "ut_gameobject.h"
#include "ut_collider.h"
#include "ut_mouse_selector.h"
#include "ut_gun_shell.h"
#include "ut_game_manager.h"
#include "ut_ship_operation.h"
#include "ut_file_reader.h"
#include "ut_hostile_controller.h"

int main(int argc, char ** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

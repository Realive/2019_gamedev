#ifndef UT_GAME_MANAGER_H
#define UT_GAME_MANAGER_H

#include "../game_manager.h"

class GameManagerTest : public ::testing::Test {
protected:
    void SetUp () {
        ships.push_back(new Destroyer("Fubuki", 100, 0, 0));
        ships.push_back(new Destroyer("Shigure", 90, 300, 300));

        gameObjects.push_back(new GunShell(100, 0, 0, 200, 200));
        gameObjects.push_back(new GunShell(0, 0, 0, 200, 200));
    }

    void TearDown () {
        for (auto it = ships.begin(); it != ships.end(); ++it)
            delete (*it);
    }

    std::list<Ship *> ships;
    std::list<GameObject *> gameObjects;
};

TEST_F (GameManagerTest, SunkManage) {
    GameManager gm(&ships, &gameObjects);

    (ships.front())->hit(120);
    gm.update();
    ASSERT_EQ(1, ships.size());
    ASSERT_EQ("Shigure", ships.front()->getName());
}

TEST_F (GameManagerTest, CleanUsedShell) {
    GameManager gm(&ships, &gameObjects);

    gm.cleanUsedShell();
    ASSERT_EQ(1, gameObjects.size());
}

#endif

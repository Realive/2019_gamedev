#include <typeinfo>

TEST (GameComponent, GetType){
    Transform * trans = new Transform(1, 2);
    GameComponent ncomp;
    GameComponent * gcomp = trans;
    ASSERT_EQ("Transform", gcomp->getType());
    ASSERT_ANY_THROW(ncomp.getType());
}

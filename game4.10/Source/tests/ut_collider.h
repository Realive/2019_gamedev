#ifndef UT_COLLIDER_H
#define UT_COLLIDER_H

#include "../game_image.h"
#include "../vector2D.h"
#include "../transform.h"
#include "../collider.h"

TEST (Collider, IsCilcked) {
    std::vector<std::string> imagePaths;
    imagePaths.push_back("path/to/image.tiff");
    Transform transform;
    GameImage gameImage(imagePaths, 1);
    gameImage.setImage(0);
    Collider collider(&transform, &gameImage);

    ASSERT_TRUE(collider.isClicked(Vector2D<int>(50, 60)));
    ASSERT_FALSE(collider.isClicked(Vector2D<int>(214, 748)));     // <--- Danny doesn't seem to like the test data :(
}

TEST (Collider, IsInSelectArea) {
    std::vector<std::string> imagePaths;
    imagePaths.push_back("path/to/image.tiff");
    Transform transform;
    GameImage gameImage(imagePaths, 1);
    gameImage.setImage(0);
    Collider collider(&transform, &gameImage);
    ASSERT_TRUE(collider.isInSelectArea(Vector2D<int>(-10, -10), Vector2D<int>(100, 100)));
    ASSERT_FALSE(collider.isInSelectArea(Vector2D<int>(-10, -10), Vector2D<int>(50, 60)));
    ASSERT_FALSE(collider.isInSelectArea(Vector2D<int>(200, 300), Vector2D<int>(400, 160)));
}

TEST (Collider, IsCollide) {
    std::vector<std::string> imagePaths;
    imagePaths.push_back("path/to/image.tiff");
    Transform transform;
    Transform transform2(50, 50);
    GameImage gameImage(imagePaths, 1);
    gameImage.setImage(0);
    Collider collider(&transform, &gameImage);
    Collider collider2(&transform2, &gameImage);
    ASSERT_TRUE(collider.isCollide(collider2));
}

TEST (Collider, GetCorners) {
    std::vector<std::string> imagePaths;
    imagePaths.push_back("path/to/image.tiff");
    Transform transform;
    GameImage gameImage(imagePaths, 1);
    gameImage.setImage(0);
    Collider collider(&transform, &gameImage);
    std::vector<Vector2D<int>> corners = collider.getCorners();
    ASSERT_EQ(Vector2D<int>(0, 0), corners[0]);
    ASSERT_EQ(Vector2D<int>(100, 0), corners[1]);
    ASSERT_EQ(Vector2D<int>(0, 100), corners[2]);
    ASSERT_EQ(Vector2D<int>(100, 100), corners[3]);
}


#endif

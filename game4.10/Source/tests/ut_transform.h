#include <math.h>

#include "../vector2D.h"
#include "../transform.h"

TEST (Transform, GetPosition) {
    Transform trans;
    Transform trans_init_position(3, 4);
    Vector2D<int> vec_none;
    Vector2D<int> vec_init(3, 4);

    ASSERT_EQ(vec_none, trans.getPosition());
    ASSERT_EQ(vec_init, trans_init_position.getPosition());
}

TEST (Transform, SetPosition) {
    Transform trans;
    Vector2D<int> newPosition(5, 6);
    trans.setPosition(newPosition);

    ASSERT_EQ(5, trans.getPosition().getX());
    ASSERT_EQ(6, trans.getPosition().getY());
}

TEST (Transform, SetDirection) {
    Transform trans;
    trans.setDirection(30);

    ASSERT_NEAR(sqrt(3)/2, trans.getDirection().getX(), 0.01);
    ASSERT_NEAR(0.5, trans.getDirection().getY(), 0.01);
}

TEST (Transform, SetDirectionByVector) {
	Transform trans;
	trans.setDirection(Vector2D<double>(1, 2));

	ASSERT_NEAR(1, trans.getDirection().getX(), 0.01);
	ASSERT_NEAR(2, trans.getDirection().getY(), 0.01);
}

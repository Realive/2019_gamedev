#ifndef C_MOVING_BITMAP_H
#define C_MOVING_BITMAP_H

#ifndef _MSC_VER

#define RGB(r,g,b) (0)

namespace game_framework {
class CMovingBitmap {
public:
    CMovingBitmap () {}
    void LoadBitmap(char str[], int rgb) {}
    void SetTopLeft(int x, int y) {}
    void ShowBitmap() {}
    int Height () {
        return 100;
    }
    int Width () {
        return 100;
    }
};
}

#endif

#endif

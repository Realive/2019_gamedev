#ifndef UT_HOSTILE_CONTROLLER_H
#define UT_HOSTILE_CONTROLLER_H

#include "../hostile_controller.h"

class HostileControllerTest : public ::testing::Test {
protected:
    HostileControllerTest(): hostileController(&ships, &gameObjects, 300) {}

    void SetUp () override {
        ships.push_back(new Destroyer("Fubuki", 2147483647, 200, 100));
		ships.push_back(new Destroyer("Fubuki", 2000, 200, 200));
		ships.push_back(new Destroyer("Fubuki", 2000, 200, 300));
		ships.push_back(new Destroyer("Fubuki", 2000, 300, 100));
		ships.push_back(new Destroyer("Fubuki", 2000, 300, 300));

        hocEnterprise = new Destroyer("Enterprise", 11000, 500, 500, false);
		ships.push_back(hocEnterprise);
        ships.push_back(new Destroyer("Wasp", 11000, 800, 800, false));
    }

    void TearDown () override {

    }

    std::list<Ship *> ships;
    std::list<GameObject *> gameObjects;
    Ship * hocEnterprise;                   // ad hoc for test conveniences
    HostileController hostileController;
};

TEST_F (HostileControllerTest, construct) {
    SUCCEED();
}

TEST_F (HostileControllerTest, GetHostileShips) {
    std::list<Ship *> hostileShips = hostileController.getHostileShips();
    ASSERT_EQ(2, hostileShips.size());
}

TEST_F (HostileControllerTest, AddDestination) {
    hostileController.setDestination(10, 10);
    hocEnterprise->setSpeed(100);
    hocEnterprise->update();
    hocEnterprise->update();
    ASSERT_NE(500, hocEnterprise->getPosition().getX());
}

TEST_F (HostileControllerTest, Patrol) {
    hostileController.addPatrolWaypoint(new Vector2D<int>(0, 0), new Vector2D<int>(500, 500));
    hostileController.patrol();
    hocEnterprise->setSpeed(100);
    hocEnterprise->update();
    hocEnterprise->update();
    ASSERT_NE(500, hocEnterprise->getPosition().getX());
}

TEST_F (HostileControllerTest, Search) {
    ASSERT_TRUE(hostileController.search(1000));
    ASSERT_FALSE(hostileController.search(100));
}

TEST_F (HostileControllerTest, Destroy) {
    FAIL();
}

#endif

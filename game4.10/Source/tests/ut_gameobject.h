#include "../gameObject.h"
#include "../Destroyer.h"
#include "../gameComponent.h"
#include "../collider.h"

class GameObjectTest : public ::testing::Test {
protected:
    void SetUp () override {
        testImagePaths.push_back("img_1.tiff");
        objects[0] = new Destroyer("fubuki", 100);
        objects[1] = new Battleship("nagato", 1000);
        objects[0]->addImage(testImagePaths, 1);
        objects[1]->addImage(testImagePaths, 1);
    }

    void TearDown () override {
        for (int i = 0; i < 2; ++i)
          delete objects[i];
    }

    GameObject * objects[2];
    std::vector<std::string> testImagePaths;
};

TEST (GameObject, GetName) {
    GameObject * gameobject = new Destroyer("DD Fubuki", 100);

    ASSERT_EQ("DD Fubuki", gameobject->getName());
}

TEST (GameObject, getGameComponentByType) {
    GameComponent * transform = new Transform(3, 4);
    GameObject * gameObject = new Destroyer("object_A", 100, 3, 4);

    Transform * component = gameObject->getComponent<Transform>();
    ASSERT_EQ(3, component->getPosition().getX());
    ASSERT_EQ(4, component->getPosition().getY());
}

TEST (GameObject, getComponentTemplate) {
    GameObject * gameObject = new Destroyer("object_A", 100, 3, 4);

    Transform * component = gameObject->getComponent<Transform>();
    ASSERT_EQ(3, component->getPosition().getX());
}

TEST_F (GameObjectTest, setPosition) {
    objects[0]->setPosition(Vector2D<int> (3, 4));
    Vector2D<int> position = objects[0]->getPosition();
    ASSERT_EQ(3, position.getX());
    ASSERT_EQ(4, position.getY());
}

TEST_F (GameObjectTest, setDirection) {
    ASSERT_EQ(0, objects[0]->getDirection().getX());
    ASSERT_EQ(1, objects[1]->getDirection().getY());

    objects[0]->setDirection(30);

    ASSERT_NEAR(0.86, objects[0]->getDirection().getX(), 0.01);
    ASSERT_NEAR(0.5, objects[0]->getDirection().getY(), 0.01);

}

TEST_F (GameObjectTest, setDirectionByVector) {
	objects[0]->setDirection(Vector2D<double>(1, 2));
	ASSERT_EQ(1, objects[0]->getDirection().getX());
	ASSERT_EQ(2, objects[0]->getDirection().getY());
}

TEST_F (GameObjectTest, IsClicked) {
    objects[0]->addComponent(new Collider(objects[0]->getComponent<Transform>(), objects[0]->getComponent<GameImage>()));
    ASSERT_TRUE(objects[0]->isClicked(Vector2D<int>(10, 10)));
    ASSERT_FALSE(objects[0]->isClicked(Vector2D<int>(101, 100)));
}
/*
TEST_F (GameObjectTest, IsInSelectArea) {
    objects[0]->addComponent(new Collider(objects[0]->getComponent<Transform>(), objects[0]->getComponent<GameImage>()));
    ASSERT_TRUE(objects[0]->IsInSelectArea(Vector2D<int>(-20, -10), Vector2D<int>(50, 50)));
}
*/

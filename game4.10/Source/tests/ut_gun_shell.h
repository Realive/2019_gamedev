#ifndef UT_GUN_SHELL_H
#define UT_GUN_SHELL_H

#include "../gun_shell.h"
#include "../collider.h"

TEST (GunShellTest, GetDamage) {
    GunShell gs(100, 100, 200, 400, 500);
    ASSERT_EQ(100, gs.getDamage());
}

TEST (GunShellTest, IsArrive) {
    GunShell gs_hit(100, 109, 150, 110, 145);
    GunShell gs_miss(100, 210, 150, 110, 150);
    ASSERT_TRUE(gs_hit.isArrive());
    ASSERT_FALSE(gs_miss.isArrive());
}

TEST (GunShellTest, Update) {
    GunShell gs(100, 109, 150, 110, 145);
    gs.update();
    ASSERT_NE(nullptr, gs.getComponent<Collider>());
}

TEST (GunShellTest, HasCollider) {
    GunShell gs(100, 109, 150, 110, 145);
    gs.update();
    ASSERT_TRUE(gs.hasCollider());
}

#endif

#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
#endif

#include <fstream>
#include <list>
#include "file_reader.h"
#include <iostream>

std::list<std::list<std::string>> fileReading(std::string path) {
	std::ifstream mapFile(path);
	std::list<std::list<std::string>> content;

	//mapFile.open(path.c_str(), std::ifstream::in);
	if (!mapFile.is_open())
		std::cout << path << std::endl;
	std::string line;
	while (std::getline(mapFile, line)) {
		std::cout << line << std::endl;
		removeSpace(line);
		std::list<std::string> row = splitElementByComma(line);
		content.push_back(row);
	}

	mapFile.close();
	return content;
}

void removeSpace(std::string & str) {
	for (auto it = str.begin(); it != str.end(); ++it) {
		if ((*it) == ' ')
			it = str.erase(it);
	}
}

std::list<std::string> splitElementByComma(std::string str) {
	std::list<std::string> elements;
	std::string delimiter = ",";

	size_t pos = 0;
	std::string token;
	while ((pos = str.find(delimiter)) != std::string::npos) {
	    elements.push_back(str.substr(0, pos));
	    str.erase(0, pos + delimiter.length());
	}
	elements.push_back(str);
	return elements;
}

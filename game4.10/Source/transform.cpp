#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
#endif

#include <math.h>
#include "transform.h"
#include "vector2D.h"

Transform::Transform (unsigned int x, unsigned int y):
	position(x, y), direction(0, 1) {}

std::string Transform::getType() {
	return (std::string)"Transform";
}

Vector2D<int> Transform::getPosition() {
    return position;
}

Vector2D<double> Transform::getDirection() {
	return direction;
}

void Transform::setPosition (const Vector2D<int> & position) {
    this->position = position;
}

void Transform::setDirection(double degree) {
	constexpr double PI = 3.141;
	double radius = degree / 360 * 2 * PI;
	direction = Vector2D<double>(cos(radius), sin(radius));
}

void Transform::setDirection(Vector2D<double> newDirection) {
	direction = newDirection;
}

#ifndef FIRE_OPERATION_H
#define FIRE_OPERATION_H

#include "ship_operation.h"

class FireOperation : public ShipOperation {
public:
    FireOperation () {}
    void operate (const std::list<Ship *> & selectedShips, std::list<GameObject *> & gameObjects, const Vector2D<int> & destination, bool isShiftDown);
};

#endif

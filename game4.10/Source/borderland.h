#ifndef BORDERLAND_H
#define BORDERLAND_H

#include <string>
#include "ship.h"
#include "gameObject.h"
#include "vector2D.h"
#include "game_image.h"

/*Four kinds of different border type, but DEFEND has no different behaviour yet*/
enum baseClass {
	NORMAL,
	DEFEND,
	REPAIR,
	MOTHER
};

class Borderland : public GameObject {
public:
	/*Borderland is a un-seen object that the game mainly about*/
	Borderland(std::string name, Vector2D<int> coordinate, Vector2D<int> area, unsigned maxHp, std::list<Ship*>* ships, baseClass baseType = NORMAL, bool isFriendly = true);
	
	// update border hp & image
	void update();

	// to check if border is still belong to friendly
	bool isBelongToFriendly();

	// get type of this border
	baseClass getBaseType();

	// get border's now Hp
	std::string getHp();

	// border will generate ship after cd goes to 0
	int getBorderCd();

	// after generating ship, reset cd
	void initBorderCd();

	// while running, cd minus 1
	void countBorderCd();

	// show border class image, with maps coordinate
	void showImage(int mapCoordinateX, int mapCoordinateY);
	
	// get borders x coordinate
	int getX(int mapCoordinateX);

	// get borders y coordinate
	int getY(int mapCoordinateY);

private:

	// set this borderland's friendly state
	void setFriendlyState(bool state);

	// construct image path string
	std::vector<std::string> initImage();
	
	// load image
	void setImage();

	bool friendly;
	unsigned maxHp;
	unsigned nowHp;
	std::string name;
	baseClass baseType;
	Vector2D<int> topLeft;
	Vector2D<int> bottomRight;
	std::list<Ship*>* ships;
	int borderCd = 1000;
};

#endif // !BORDERLAND_H
#include <list>
#include "Vector2D.h"

namespace game_framework {
	class GameMap {
	public:
		/*Game map is the background we see, do less, but important*/
		GameMap();
		~GameMap();

		// load image chunks
		void LoadBitmap();

		// set map moving behaviour
		void onKeyDown(UINT nChar);

		// init map moving behaviour
		void onKeyUp(UINT nChar);

		// updata map position
		void mapCoordinateUpdate();

		// refresh map chunks
		void refreshChunkStatus();
		
		// showing map images
		void OnShow();

		// set map moving behaviour by mouse
		void OnMouseMove(int, int);

		// stop map moving
		void stopMapMove();

		// get map actual X coordinate
		int getMapCoordX();

		// get map actual Y coordinate
		int getMapCoordY();
		Vector2D<int> getMapCoord() const;

	private:
		// init map chunks path
		char * pathInitializer(int i);

		// load map chunks images
		void initChunkStatus();

		int mapCoordinateX;
		int mapCoordinateY;
		const int chunkWidth = 240;
		const int chunkHeight = 231;
		static const int chunkRowNumber = 6;
		static const int chunkColumnNumber = 10;


		int chunkShowStatus[chunkRowNumber][chunkColumnNumber];

		std::vector<CMovingBitmap> mapLarge;

		bool isMovingDown = false;
		bool isMovingUp = false;
		bool isMovingLeft = false;
		bool isMovingRight = false;

		const int moveMapSpeed = 10;
		const int moveMargin = 100;
	};
}
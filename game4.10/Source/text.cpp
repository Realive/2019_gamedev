#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
	#include "gamelib.h"
#endif
#include <string>

#include "text.h"

Text::Text(std::string textContent, Vector2D<int> textCoordinate) : textContent(textContent), textCoordinate(textCoordinate) {}

void Text::show() {
	CDC *pDC = game_framework::CDDraw::GetBackCDC();
	CFont f, *fp;
	fp = pDC->SelectObject(&f);
	pDC->SetBkColor(RGB(rgbBkColor[0], rgbBkColor[1], rgbBkColor[2]));
	pDC->SetTextColor(RGB(rgbTextColor[0], rgbTextColor[1], rgbTextColor[2]));
	pDC->TextOut((int)textCoordinate.getX(), (int)textCoordinate.getY(), textContent.c_str());
	pDC->SelectObject(fp);
	game_framework::CDDraw::ReleaseBackCDC();
}

void Text::setText(std::string text) {
	textContent = text;
}

void Text::setColor(int r, int g, int b) {
	rgbTextColor[0] = r;
	rgbTextColor[1] = g;
	rgbTextColor[2] = b;
}

void Text::setBkColor(int r, int g, int b) {
	rgbBkColor[0] = r;
	rgbBkColor[1] = g;
	rgbBkColor[2] = b;
}

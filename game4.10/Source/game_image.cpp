#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
	#include "gamelib.h"
#else
	#include "tests/mock/c_moving_bitmap.h"
#endif

#include "game_image.h"

GameImage::GameImage(std::vector<std::string> imagePaths, size_t imageNumber) {
	for (size_t i = 0; i < imageNumber; ++i) {
		game_framework::CMovingBitmap* bitmap = new game_framework::CMovingBitmap();
		Std2CString std2CString;
		bitmap->LoadBitmap(std2CString(imagePaths[i]), RGB(255, 255, 255));
		images.push_back(bitmap);
	}
}

GameImage::~GameImage() {
	for (auto it = images.begin(); it != images.end(); ++it)
		delete (*it);
}

void GameImage::setImage(unsigned int imageId) {
	currentImageId = imageId;
}

/* The position should be screen position, coordinate should be converted at gameObject */
void GameImage::showImage(int screenCoordX, int screenCoordY) const {
	if (screenCoordX >= 0 && screenCoordX <= SIZE_X && screenCoordY >= 0 && screenCoordY <= SIZE_Y) {
		images[currentImageId]->SetTopLeft(screenCoordX, screenCoordY);
		images[currentImageId]->ShowBitmap();
	}
}

std::string GameImage::getType() const {
	return "GameImage";
}

int GameImage::getImageHeight () const {
	return images[currentImageId]->Height();
}

int GameImage::getImageWidth () const {
	return images[currentImageId]->Width();
}

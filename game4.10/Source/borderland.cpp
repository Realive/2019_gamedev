#include "StdAfx.h"
	#ifdef _MSC_VER
#include <ddraw.h>
#endif

#include <string>
#include "score.h"
#include "borderland.h"
#include "vector2D.h"

Borderland::Borderland(std::string name, Vector2D<int> topLeft, Vector2D<int> bottomRight, unsigned maxHp, std::list<Ship*>* ships, baseClass baseType, bool isFriendly) : GameObject(name) {
	this->topLeft = topLeft;
	this->bottomRight = bottomRight;
	this->maxHp = maxHp;
	this->nowHp = maxHp;
	this->ships = ships;
	this->friendly = isFriendly;
	switch (baseType)
	{
	case NORMAL:
		this->baseType = NORMAL;
		break;
	case DEFEND:
		this->baseType = DEFEND;
		break;
	case REPAIR:
		this->baseType = REPAIR;
		break;
	case MOTHER:
		this->baseType = MOTHER;
		break;
	default:
		break;
	}
	addComponent(new GameImage(initImage(), 8));
	addComponent(new Transform(topLeft.getX(), topLeft.getY()));
}

void Borderland::update() {
	unsigned friendlyCounter = 0;
	unsigned enemyCounter = 0;
	for (auto it = this->ships->begin(); it != this->ships->end(); ++it) {
		Vector2D<int> shipCord = (*it)->getPosition();
		if (shipCord.getX() > this->topLeft.getX() && shipCord.getX() < this->bottomRight.getX() &&
			shipCord.getY() > this->topLeft.getY() && shipCord.getY() < this->bottomRight.getY()) {
			if ((*it)->isFriendly()) {
				if (baseType == REPAIR && friendly == true)
					(*it)->hpBuff();
				friendlyCounter++;
			}
			else {
				if (baseType == REPAIR && friendly == false)
					(*it)->hpBuff();
				enemyCounter++;
			}
		}
	}

	if (this->friendly == true && friendlyCounter > enemyCounter) {
		nowHp += friendlyCounter - enemyCounter;
		if (nowHp > maxHp)
			nowHp = maxHp;
	}
	else if (this->friendly == true && friendlyCounter < enemyCounter) {
		nowHp -= enemyCounter - friendlyCounter;
		if (nowHp > maxHp)
			nowHp = 0;
		if (nowHp == 0)
			setFriendlyState(false);
	}
	else if (this->friendly == false && friendlyCounter > enemyCounter) {
		nowHp -= friendlyCounter - enemyCounter;
		if (nowHp > maxHp)
			nowHp = 0;
		if (nowHp == 0) {
			setFriendlyState(true);
			Score::getInstance()->addString(this->getName());
			Score::getInstance()->addScore(100);
		}
	} else if (this->friendly == false && friendlyCounter < enemyCounter) {
		nowHp += enemyCounter - friendlyCounter;
		if (nowHp > maxHp)
			nowHp = maxHp;
	}
	setImage();
}

void Borderland::setImage() {
	switch (baseType)
	{
	case NORMAL:
		if (isBelongToFriendly() == true)
			getComponent<GameImage>()->setImage(0);
		else
			getComponent<GameImage>()->setImage(1);
		break;
	case DEFEND:
		if (isBelongToFriendly() == true)
			getComponent<GameImage>()->setImage(2);
		else
			getComponent<GameImage>()->setImage(3);
		break;
	case REPAIR:
		if (isBelongToFriendly() == true)
			getComponent<GameImage>()->setImage(4);
		else
			getComponent<GameImage>()->setImage(5);
		break;
	case MOTHER:
		if (isBelongToFriendly() == true)
			getComponent<GameImage>()->setImage(6);
		else
			getComponent<GameImage>()->setImage(7);
		break;
	default:
		if (isBelongToFriendly() == true)
			getComponent<GameImage>()->setImage(0);
		else
			getComponent<GameImage>()->setImage(1);
		break;
	}
}

void Borderland::showImage(int mapCoordinateX, int mapCoordinateY) {
	try {
		GameImage * gameImage = getComponent<GameImage>();
		gameImage->showImage(mapCoordinateX + ((topLeft.getX() + bottomRight.getX() - gameImage->getImageWidth()) / 2), mapCoordinateY + ((topLeft.getY() + bottomRight.getY() - gameImage->getImageHeight()) / 2));
	}
	catch (std::string e) {

	}
}

std::vector<std::string> Borderland::initImage() {
	std::vector<std::string> imagePaths;
	imagePaths.push_back("Bitmaps/normal_b.bmp");
	imagePaths.push_back("Bitmaps/normal_r.bmp");
	imagePaths.push_back("Bitmaps/defend_b.bmp");
	imagePaths.push_back("Bitmaps/defend_r.bmp");
	imagePaths.push_back("Bitmaps/repair_b.bmp");
	imagePaths.push_back("Bitmaps/repair_r.bmp");
	imagePaths.push_back("Bitmaps/mother_b.bmp");
	imagePaths.push_back("Bitmaps/mother_r.bmp");
	return imagePaths;
}

bool Borderland::isBelongToFriendly() {
	return friendly;
}

std::string Borderland::getHp() {
	return std::to_string(nowHp) + " / " + std::to_string(maxHp);
}

baseClass Borderland::getBaseType() {
	return this->baseType;
}

void Borderland::setFriendlyState(bool state) {
	if (state == true)
		this->friendly = true;
	else
		this->friendly = false;
}

int Borderland::getBorderCd() {
	return borderCd;
}

void Borderland::initBorderCd() {
	if (friendly == true)
		borderCd = 1000;
	else
		borderCd = 2000;
}

void Borderland::countBorderCd() {
	borderCd--;
}

int Borderland::getX(int randX) {
	GameImage * gameImage = getComponent<GameImage>();
	return ((topLeft.getX() + bottomRight.getX() - gameImage->getImageWidth()) / 2) + randX;
}

int Borderland::getY(int randY) {
	GameImage * gameImage = getComponent<GameImage>();
	return ((topLeft.getY() + bottomRight.getY() - gameImage->getImageHeight()) / 2) + randY;
}
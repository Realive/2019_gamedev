#ifndef HOSTILE_CONTROLLER_H
#define HOSTILE_CONTROLLER_H

#include <list>

#include "Ship.h"
#include "gameObject.h"

class HostileController {
public:
	/*A game must have some bad guys isn't it?*/
    HostileController(std::list<Ship *> * ships, std::list<GameObject *> * gameObjects, int searchRange);
    ~HostileController();

	// set enemy destination
    void setDestination(int x, int y);
    
	// let com patrol in two points
	void addPatrolWaypoint(Vector2D<int> * pointA, Vector2D<int> * pointB);
    
	// do patrol, danger close
	void patrol();

	// if pc close, shoot it down
    void searchAndDestroy();    // the order given to the hostile ship, onMove
    
	// attack
	void destroy();

	// leave it along
    void keepInRange(int range);
    
	// set HVT
	void setTarget();

	// find target in range
    bool search(int range) const;

	// get all brothers in arm
    std::list<Ship *> getHostileShips () const;
private:
    Vector2D<int> * wayPointA;
    Vector2D<int> * wayPointB;
    std::list<Ship *> * ships;
    std::list<GameObject *> * gameObjects;
    Ship * target = nullptr;
    int searchRange;
};

#endif

#ifndef SHIP_OPERATION_H
#define SHIP_OPERATION_H

#include "mouse_selector.h"

class ShipOperation {
public:
	// ship behaviour while mouse click
    void mouseLClickDown (int x, int y);
	virtual void operate (const std::list<Ship *> & selectedShips, std::list<GameObject *> & gameObjects, const Vector2D<int> & destination, bool isShiftDown) = 0;
private:
    int lDownX, lDownY;
};

#endif

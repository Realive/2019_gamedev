#ifndef UI_H
#define UI_H

class UI {
public:
	virtual void show () = 0;
};

#endif

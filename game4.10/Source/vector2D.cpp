#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
#endif

#include <math.h>
#include "vector2D.h"

template<class T>
Vector2D<T>::Vector2D() {
	x = 0;
	y = 0;
}

template<class T>
Vector2D<T>::Vector2D(T x, T y) {
	this->x = x;
	this->y = y;
}

template<class T>
Vector2D<T>::Vector2D(const Vector2D<T> & vec) {
	x = vec.x;
	y = vec.y;
}

template<class T>
T Vector2D<T>::getX() const {
	return x;
}

template<class T>
T Vector2D<T>::getY() const {
	return y;
}

template<class T>
double Vector2D<T>::getLength() const {
	return sqrt(pow(x, 2) + pow(y, 2));
}

template<class T>
Vector2D<T> Vector2D<T>::inverse() {
	return Vector2D<T>(-x, -y);
}

template<class T>
Vector2D<double> Vector2D<T>::getUnitVector () const {
	return Vector2D<double> (x / getLength(), y / getLength());
}

template<class T>
Vector2D<T> Vector2D<T>::operator + (Vector2D<T>& vector) {
	return Vector2D<T> (x + vector.getX(), y + vector.getY());
}

template<class T>
bool Vector2D<T>::operator == (const Vector2D<T>& rhs) const {
	if (x == rhs.x && y == rhs.y)
		return true;
	return false;
}

Vector2D<int> operator + (const Vector2D<int> & lhs, const Vector2D<double> & rhs) {
	return Vector2D<int>(lhs.getX() + (int)round(rhs.getX()), lhs.getY() + (int)round(rhs.getY()));
}

Vector2D<double> operator * (const Vector2D<double> & vec, int multiplier) {
	return Vector2D<double>(vec.getX() * multiplier, vec.getY() * multiplier);
}

Vector2D<int> operator - (const Vector2D<int> & lhs, const Vector2D<int> & rhs) {
	return Vector2D<int> (lhs.getX() - rhs.getX(), lhs.getY() - rhs.getY());
}

int distance(const Vector2D<int> & lhs, const Vector2D<int> & rhs) {
	return (int)(lhs - rhs).getLength();
}
template class Vector2D<int>;
template class Vector2D<double>;

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <string>
#include <list>

#include "gameComponent.h"
#include "transform.h"
#include "game_image.h"
#include "collider.h"

class GameObject {
public:
	/*Everything is a object*/
    GameObject(std::string objectName);
    GameObject(std::string objectName, int initPositionX, int initPositionY);

    virtual ~GameObject();
	virtual void update() = 0;

	// get object individual name
    std::string getName() const;

	// add component under object
    void addComponent(GameComponent * component);
	
	// add images
	void addImage(std::vector<std::string> imagePaths, size_t imageNum);

	// set object position
	void setPosition(const Vector2D<int> & newPosition);
	
	// set object heading position
	void setDirection(int degree);

	// set object waypoint
	void setDirection(const Vector2D<double> & newDirection);
	
	// show object image
	void showImage(int mapCoordinateX, int mapCoordinateY);
    
	// to check if object is clicked
	bool isClicked(const Vector2D<int> & mousePosition);
	
	// to check if object is in selected area
	bool isInSelectArea(const Vector2D<int> & mouseTopLeft, const Vector2D<int> & mouseBottomRight);
	
	// get object position
	Vector2D<int> getPosition();
	
	// get object heading position
	Vector2D<double> getDirection();
	
	// get object's collider
	Collider getCollider();

	// get <component type> component
    template<class ComponentType> ComponentType * getComponent ();

private:
    std::string name;
    std::list<GameComponent *> gameComponents;
};

#endif

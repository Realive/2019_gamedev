#ifndef COLLIDER_CONTROLLER_H
#define COLLIDER_CONTROLLER_H

#include <list>
#include "Ship.h"

class ColliderController {
public:
	/*To define collition behaviour*/
	ColliderController(std::list<Ship *> * ships, std::list<GameObject *> * gameObjects);

	// to see if any things is colliding
	void update();

	// ship's collision, should bounce
	void shipCollision();

	// shell's collition, should cause damadge
	void shellCollision();

	// actual place to define bouncing behaviour
	void collideBounce(Ship * ship, Ship * collided);
private:
	std::list<Ship *> * ships;
	std::list<GameObject *> * gameObjects;
};

#endif

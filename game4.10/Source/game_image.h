#ifndef GAME_IMAGE_H
#define GAME_IMAGE_H

#include <list>
#include <vector>
#include <string>

#ifdef _MSC_VER
	#include "gamelib.h"
#else
	#include "tests/mock/c_moving_bitmap.h"
	#define SIZE_X 640
	#define SIZE_Y 480
#endif

#include "gameComponent.h"
#include "convert_extension.h"
#include "vector2D.h"

class GameImage : public GameComponent {
public:
	/*Game image will pack CMovingBitmaps and load only once*/
	GameImage(std::vector<std::string> imagePaths, size_t imageNumber);
	~GameImage();

	// set object's image in the n-th
	void setImage(unsigned int imageId);
	
	/* The position should be screen position, coordinate should be converted at gameObject */
	// show object's image
	void showImage(int screenCoordX, int screenCoordY) const;
	
	// get component <GameImage>
	std::string getType() const;

	// get now setted image's height
	int getImageHeight() const;

	// get now setted image's width
	int getImageWidth() const;
private:
	unsigned int currentImageId = 0;
	std::vector<game_framework::CMovingBitmap*> images;
};


#endif

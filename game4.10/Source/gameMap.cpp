#include "stdafx.h"
#include "Resource.h"
#include <vector>
#include <mmsystem.h>
#include <ddraw.h>
#include <string>

#include "gamelib.h"
#include "gameMap.h"

namespace game_framework {
	GameMap::GameMap() :mapCoordinateX(0), mapCoordinateY(0) {

	}

	GameMap::~GameMap() {

	}

	void GameMap::LoadBitmap() {
		initChunkStatus();
		for (unsigned int i = 1; i <= (chunkRowNumber * chunkColumnNumber); ++i) {
			CMovingBitmap bitmap;
			bitmap.LoadBitmap(pathInitializer(i));
			mapLarge.push_back(bitmap);
		}
	}

	char* GameMap::pathInitializer(int i) {
		std::string path = "Bitmaps/map_6_10/";
		path.append(std::to_string(i));
		path.append(".bmp");
		char * cPath = new char[path.size()];
		strcpy(cPath, path.c_str());
		return cPath;
	}

	void GameMap::stopMapMove() {
		isMovingDown = isMovingUp = isMovingLeft = isMovingRight = false;
	}

	void GameMap::OnMouseMove(int mouseX, int mouseY) {
		stopMapMove();
		if (mouseX < moveMargin)
			isMovingRight = true;
		if (mouseX > SIZE_X - moveMargin)
			isMovingLeft = true;
		if (mouseY < moveMargin)
			isMovingDown = true;
		if (mouseY > SIZE_Y - moveMargin)
			isMovingUp = true;
	}

	/*
	 * TODO: Refine this method, this approach is idiotic >:(
	 */
	void GameMap::onKeyDown(UINT nChar) {
		const char KEY_LEFT = 0x25;		// keyboard left arrow
		const char KEY_UP = 0x26;		// keyboard up arrow
		const char KEY_RIGHT = 0x27;	// keyboard right arrow
		const char KEY_DOWN = 0x28;		// keyboard down arrow
		if (nChar == KEY_LEFT)
			isMovingLeft = true;
		if (nChar == KEY_RIGHT)
			isMovingRight = true;
		if (nChar == KEY_UP)
			isMovingUp = true;
		if (nChar == KEY_DOWN)
			isMovingDown = true;
	}

	/*
	* TODO: Refine this method, this approach is idiotic >:(
	*/
	void GameMap::onKeyUp(UINT nChar) {
		const char KEY_LEFT = 0x25;		// keyboard left arrow
		const char KEY_UP = 0x26;		// keyboard up arrow
		const char KEY_RIGHT = 0x27;	// keyboard right arrow
		const char KEY_DOWN = 0x28;		// keyboard down arrow
		if (nChar == KEY_LEFT)
			isMovingLeft = false;
		if (nChar == KEY_RIGHT)
			isMovingRight = false;
		if (nChar == KEY_UP)
			isMovingUp = false;
		if (nChar == KEY_DOWN)
			isMovingDown = false;
	}

	void GameMap::mapCoordinateUpdate() {
		if (isMovingUp) {
			mapCoordinateY -= moveMapSpeed;
			if (mapCoordinateY <= -(chunkRowNumber * mapLarge[0].Height() - 720))
				mapCoordinateY = -(chunkRowNumber * mapLarge[0].Height() - 720);
		}
		if (isMovingDown) {
			mapCoordinateY += moveMapSpeed;
			if (mapCoordinateY >= 0)
				mapCoordinateY = 0;
		}
		if (isMovingLeft) {
			mapCoordinateX -= moveMapSpeed;
			if (mapCoordinateX <= -(chunkColumnNumber * mapLarge[0].Width() - 1280))
				mapCoordinateX = -(chunkColumnNumber * mapLarge[0].Width() - 1280);
		}
		if (isMovingRight) {
			mapCoordinateX += moveMapSpeed;
			if (mapCoordinateX >= 0)
				mapCoordinateX = 0;
		}
	}

	/*HOW CAN THE SPEED OF UPDATE BE SO FAST???*/
	void GameMap::OnShow() {
		refreshChunkStatus();
		mapCoordinateUpdate();
		for (int i = 0; i < 60; ++i) {
			int row = i % 10;
			int col = i / 10;
			if (chunkShowStatus[col][row] == 1) {
				mapLarge[i].SetTopLeft(row * chunkWidth + mapCoordinateX, col * chunkHeight + mapCoordinateY);
				mapLarge[i].ShowBitmap();
			}
		}
	}

	void GameMap::refreshChunkStatus() {
		for (int i = 0; i < chunkColumnNumber; ++i) {
			for (int j = 0; j < chunkRowNumber; ++j) {
				int chunkCoordinateX = i * chunkWidth + mapCoordinateX;
				int chunkCoordinateY = j * chunkHeight + mapCoordinateY;
				if (chunkCoordinateX < -chunkWidth || chunkCoordinateX > SIZE_X ||
					chunkCoordinateY < -chunkHeight || chunkCoordinateY > SIZE_Y) {
					chunkShowStatus[j][i] = 0;
				}
				else {
					chunkShowStatus[j][i] = 1;
				}
			}
		}
	}

	void GameMap::initChunkStatus() {
		for (size_t i = 0; i < chunkRowNumber; ++i)
			for (size_t j = 0; j < chunkColumnNumber; ++j)
				chunkShowStatus[i][j] = 0;
	}

	int GameMap::getMapCoordX() {
		return mapCoordinateX;
	}

	int GameMap::getMapCoordY() {
		return mapCoordinateY;
	}

	Vector2D<int> GameMap::getMapCoord() const {
		return Vector2D<int>(mapCoordinateX, mapCoordinateY);
	}
}

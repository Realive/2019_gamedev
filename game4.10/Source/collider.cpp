#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
	#include "gamelib.h"
#endif

#include "collider.h"

Collider::Collider (Transform * transform, GameImage * gameImage) {
    this->transform = transform;
    this->image = gameImage;
}

bool Collider::isClicked (Vector2D<int> mousePosition) const {
    int rightBorder = transform->getPosition().getX() + image->getImageWidth();
    int bottomBorder = transform->getPosition().getY() + image->getImageHeight();

    if (mousePosition.getX() >= transform->getPosition().getX() && mousePosition.getX() <= rightBorder &&
        mousePosition.getY() >= transform->getPosition().getY() && mousePosition.getY() <= bottomBorder)
        return true;
    return false;
}

bool Collider::isInSelectArea(const Vector2D<int> & mousePointA, const Vector2D<int> & mousePointB) const {
	Vector2D<int> objectTopLeft(transform->getPosition().getX(), transform->getPosition().getY());
	Vector2D<int> objectBottomRight(objectTopLeft.getX() + image->getImageWidth(), objectTopLeft.getY() + image->getImageHeight());

	int selectLeft = (mousePointA.getX() < mousePointB.getX()) ? mousePointA.getX() : mousePointB.getX();
	int selectTop = (mousePointA.getY() < mousePointB.getY()) ? mousePointA.getY() : mousePointB.getY();
	int selectRight = (mousePointA.getX() > mousePointB.getX()) ? mousePointA.getX() : mousePointB.getX();
	int selectBottom = (mousePointA.getY() > mousePointB.getY()) ? mousePointA.getY() : mousePointB.getY();

	if ((selectLeft <= objectTopLeft.getX() && selectRight >= objectBottomRight.getX()) &&
		(selectTop <= objectTopLeft.getY() && selectBottom >= objectBottomRight.getY()))
		return true;
	return false;
}

bool Collider::isCollide(const Collider & collider) const {
	std::vector<Vector2D<int>> selfCorners = getCorners();
	std::vector<Vector2D<int>> colliderCorners = collider.getCorners();
	for (size_t i = 0; i < colliderCorners.size(); ++i) {
		if (selfCorners[0].getX() < colliderCorners[i].getX() && selfCorners[3].getX() > colliderCorners[i].getX() &&
			selfCorners[0].getY() < colliderCorners[i].getY() && selfCorners[3].getY() > colliderCorners[i].getY())
			return true;
	}
	return false;
}

std::vector<Vector2D<int>> Collider::getCorners() const {
	std::vector<Vector2D<int>> corners;
	int positionX = transform->getPosition().getX();
	int positionY = transform->getPosition().getY();
	corners.push_back(Vector2D<int>(positionX, positionY));
	corners.push_back(Vector2D<int>(positionX + image->getImageWidth(), positionY));
	corners.push_back(Vector2D<int>(positionX, positionY + image->getImageHeight()));
	corners.push_back(Vector2D<int>(positionX + image->getImageWidth(), positionY + image->getImageHeight()));
	return corners;
}

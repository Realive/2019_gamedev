#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
#endif

#include "collider_controller.h"
#include "collider.h"
#include "gun_shell.h"

ColliderController::ColliderController(std::list<Ship *> * ships, std::list<GameObject *> * gameObjects) {
	this->ships = ships;
	this->gameObjects = gameObjects;
}

void ColliderController::update () {
	shipCollision();
	shellCollision();
}

void ColliderController::shipCollision () {
	/* check object collision */
	for (auto it = ships->begin(); it != --(ships->end()); ++it) {
		for (auto itNext = it; itNext != ships->end(); ++itNext) {
			if ((*it)->getCollider().isCollide((*itNext)->getCollider())) {
				collideBounce(*it, *itNext);
				collideBounce(*itNext, *it);
			}
		}
	}
}

void ColliderController::shellCollision () {
	std::list<GunShell *> shells;
	for (auto it = gameObjects->begin(); it != gameObjects->end(); ++it) {
		if (dynamic_cast<GunShell *>(*it) != nullptr) {
			GunShell * shell = dynamic_cast<GunShell *>(*it);
			if (shell->hasCollider()) {
				shells.push_back(shell);
			}
		}
	}
	for (auto it = ships->begin(); it != ships->end(); ++it) {
		for (auto shell = shells.begin(); shell != shells.end(); ++shell) {
			if ((*it)->getCollider().isCollide((*shell)->getCollider())) {
				(*it)->hit((*shell)->getDamage());
				(*shell)->hasCausedDamage();		// change the damage of shell to 0
			}
		}
		
	}
}

void ColliderController::collideBounce(Ship * ship, Ship * collided) {
	ship->setSpeed(3);
	ship->setDirection((ship->getPosition() - collided->getPosition()).getUnitVector());
	ship->update();
}
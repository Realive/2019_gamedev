#ifndef COLLIDER_H
#define COLLIDER_H

#include "gameComponent.h"
#include "transform.h"
#include "game_image.h"

class Collider : public GameComponent {
public:
	/*Collider detect whether two object contact, and deal with the option contact behaviour*/
    Collider (Transform * transform, GameImage * gameImage);

	// to see if mouse clicked a ship
    bool isClicked (Vector2D<int> mousePosition) const;
	
	// to see if the ships are in the L-Button down and up area
	bool isInSelectArea(const Vector2D<int> & mousePointA, const Vector2D<int> & mousePointB) const;

	// to detect two object is collided
    bool isCollide(const Collider & collider) const;

	// get objects actual position
    std::vector<Vector2D<int>> getCorners() const;
private:
    Transform * transform = nullptr;
    GameImage * image = nullptr;
};

#endif

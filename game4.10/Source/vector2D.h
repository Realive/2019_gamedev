#ifndef VECTOR_2D_H
#define VECTOR_2D_H

template<class T>
class Vector2D {
public:
	/*Pack anything with 2 coordinate into a object*/
	Vector2D();
	Vector2D(T x, T y);
 	Vector2D(const Vector2D<T> & vec);

	// get x coordinate
	T getX() const;	/* TODO :) change to return by value */
	
	// get y coordinate
	T getY() const;

	// get vector's length
	double getLength () const;

	// minus vector
	Vector2D<T> inverse ();

	// get unit vector
	Vector2D<double> getUnitVector () const;

	// + operation
	Vector2D<T> operator + (Vector2D<T>& vector);

	// == operation
	bool operator == (const Vector2D<T>& rhs) const;
private:
	T x = 0;
	T y = 0;
};

Vector2D<int> operator + (const Vector2D<int> & lhs, const Vector2D<double> & rhs);
Vector2D<double> operator * (const Vector2D<double> & vec, int multiplier);
Vector2D<int> operator - (const Vector2D<int> & lhs, const Vector2D<int> & rhs);
int distance(const Vector2D<int> & lhs, const Vector2D<int> & rhs);

#endif

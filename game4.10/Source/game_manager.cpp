#include "StdAfx.h"
#ifdef _MSC_VER
    #include <ddraw.h>
#endif

#include "game_manager.h"
#include "gun_shell.h"
#include <iostream>

GameManager::GameManager (std::list<Ship *> * ship, std::list<GameObject *> * gameObject) {
    ships = ship;
	gameObjects = gameObject;
}

void GameManager::update() {
	cleanUsedShell();
    for (auto it = ships->begin(); it != ships->end(); ++it) {
		if ((*it)->isSunk()) {
			it = ships->erase(it);
			break;
		}
    }
}

void GameManager::cleanUsedShell() {
	for (auto it = gameObjects->begin(); it != gameObjects->end(); ++it) {
		GunShell * shell = dynamic_cast<GunShell *>(*it);
		if (shell != nullptr && shell->getDamage() == 0) {
			it = gameObjects->erase(it);
			break;
		}
	}
}

bool GameManager::isWin(Borderland *border) {
	if (border->getBaseType() == MOTHER && border->isBelongToFriendly() == true)
		return true;
	else
		return false;
}

bool GameManager::isLose(Borderland *border) {
	if (border->getBaseType() == MOTHER && border->isBelongToFriendly() == false)
		return true;
	else
		return false;
}
#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
#endif

#include "fire_operation.h"
#include "gun_shell.h"

void FireOperation::operate (const std::list<Ship *> & selectedShips, std::list<GameObject *> & gameObjects, const Vector2D<int> & destination, bool isShiftDown) {
    for (auto it = selectedShips.begin(); it != selectedShips.end(); ++it) {
		if ((*it)->isFriendly() && !(*it)->isInCd()) {
			(*it)->fireShell();
			Vector2D<int> shipPosition = (*it)->getPosition();
			gameObjects.push_back(new GunShell(10, shipPosition.getX(), shipPosition.getY(), destination.getX(), destination.getY()));
		}
	}
}

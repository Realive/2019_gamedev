#ifndef BORDERLAND_CONTROLLER
#define BORDERLAND_CONTROLLER

#include "gameObject.h"
#include "vector2D.h"

class BorderlandController : public GameObject {
public:
	BorderlandController();
private:
	std::list<Border> borders;
};

#endif //  BORDERLAND_CONTROLLER

#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
#endif

#include "move_operation.h"
#include "mouse_selector.h"
#include "convert_extension.h"

void MoveOperation::operate (const std::list<Ship *> & selectedShips, std::list<GameObject *> & gameObjects, const Vector2D<int> & destination, bool isShiftDown) {
    for (auto it = selectedShips.begin(); it != selectedShips.end(); ++it) {
		if ((*it)->isFriendly()) {
			(*it)->addDestination(destination ,isShiftDown);
			(*it)->setSpeed(3);
		}
	}
}

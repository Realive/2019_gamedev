#ifndef TEXT_H
#define TEXT_H

#ifdef _MSC_VER
	#include <ddraw.h>
#endif
#include "ui.h"
#include "vector2D.h"

class Text : public UI {
public:
	/*Conbine string and coordinate together*/
	Text(std::string textContent, Vector2D<int> textCoordinate);
	
	// to show text
	void show();

	// to set text string
	void setText(std::string text);
	
	// to set string color
	void setColor(int r, int g, int b);
	
	// to set background color
	void setBkColor(int r, int g, int b);
private:
	std::string textContent;
	std::string fontType = "Times New Roman";
	Vector2D<int> textCoordinate;
	int rgbBkColor [3] = { 0, 0, 0 };
	int rgbTextColor [3] = { 255, 255, 255 };
};

#endif

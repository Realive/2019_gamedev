/*
 * mygame.cpp: 本檔案儲遊戲本身的class的implementation
 * Copyright (C) 2002-2008 Woei-Kae Chen <wkc@csie.ntut.edu.tw>
 *
 * This file is part of game, a free game development framework for windows.
 *
 * game is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * game is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * History:
 *   2002-03-04 V3.1
 *          Add codes to demostrate the use of CMovingBitmap::ShowBitmap(CMovingBitmap &).
 *	 2004-03-02 V4.0
 *      1. Add CGameStateInit, CGameStateRun, and CGameStateOver to
 *         demonstrate the use of states.
 *      2. Demo the use of CInteger in CGameStateRun.
 *   2005-09-13
 *      Rewrite the codes for CBall and CEraser.
 *   2005-09-20 V4.2Beta1.
 *   2005-09-29 V4.2Beta2.
 *      1. Add codes to display IDC_GAMECURSOR in GameStateRun.
 *   2006-02-08 V4.2
 *      1. Revise sample screens to display in English only.
 *      2. Add code in CGameStateInit to demo the use of PostQuitMessage().
 *      3. Rename OnInitialUpdate() -> OnInit().
 *      4. Fix the bug that OnBeginState() of GameStateInit is not called.
 *      5. Replace AUDIO_CANYON as AUDIO_NTUT.
 *      6. Add help bitmap to CGameStateRun.
 *   2006-09-09 V4.3
 *      1. Rename Move() and Show() as OnMove and OnShow() to emphasize that they are
 *         event driven.
 *   2006-12-30
 *      1. Bug fix: fix a memory leak problem by replacing PostQuitMessage(0) as
 *         PostMessage(AfxGetMainWnd()->m_hWnd, WM_CLOSE,0,0).
 *   2008-02-15 V4.4
 *      1. Add namespace game_framework.
 *      2. Replace the demonstration of animation as a new bouncing ball.
 *      3. Use ShowInitProgress(percent) to display loading progress. 
 *   2010-03-23 V4.6
 *      1. Demo MP3 support: use lake.mp3 to replace lake.wav.
*/

#include "stdafx.h"
#include "Resource.h"
#include <mmsystem.h>
#include <ddraw.h>
#include <stdlib.h>
#include <time.h>
#include "score.h"
#include "audio.h"
#include "gamelib.h"
#include "mygame.h"
#include "text.h"
#include "Destroyer.h"
#include "Battleship.h"
#include "gun_shell.h"
#include "move_operation.h"
#include "fire_operation.h"
#include "borderland.h"

namespace game_framework {
	/////////////////////////////////////////////////////////////////////////////
	// 這個class為遊戲的遊戲開頭畫面物件
	/////////////////////////////////////////////////////////////////////////////

	CGameStateInit::CGameStateInit(CGame *g)
		: CGameState(g)
	{
		
	}

	void CGameStateInit::OnInit()
	{
		ShowInitProgress(0);	// 一開始的loading進度為0%
	
		titleImage.LoadBitmap("Bitmaps/title.bmp", RGB(0, 0, 0));
		startImage.LoadBitmap("Bitmaps/start.bmp", RGB(0, 0, 0));
		tutorialImage.LoadBitmap("Bitmaps/tutorial.bmp", RGB(0, 0, 0));

		ShowInitProgress(30);

		startImage.SetTopLeft(640 - (startImage.Width() / 2), titleImage.Top() + 480);
		titleText.push_back(new Text("Press ESC to Exit", Vector2D<int>(570, 600)));
		tutorialImage.SetTopLeft(0, 400);

		ShowInitProgress(60);

		CAudio::Instance()->Load(BGM_TITLE, "Sounds/Title.mp3");
		CAudio::Instance()->Load(BGM_MAIN, "Sounds/main.mp3");
		CAudio::Instance()->Load(BGM_WIN, "Sounds/win.mp3");
		CAudio::Instance()->Load(BGM_LOSE, "Sounds/lose.mp3");
		CAudio::Instance()->Play(BGM_TITLE, true);

		ShowInitProgress(100);
	}

	void CGameStateInit::OnBeginState()
	{
	}

	void CGameStateInit::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
	{
		const char KEY_ESC = 27;
		const char KEY_SPACE = ' ';
		if (nChar == KEY_SPACE) {
			CAudio::Instance()->Stop(BGM_TITLE);
			CAudio::Instance()->Play(BGM_MAIN, true);
			GotoGameState(GAME_STATE_RUN);		}				// 切換至GAME_STATE_RUN
		else if (nChar == KEY_ESC)								// Demo 關閉遊戲的方法
			PostMessage(AfxGetMainWnd()->m_hWnd, WM_CLOSE, 0, 0);	// 關閉遊戲
	}

	void CGameStateInit::OnLButtonDown(UINT nFlags, CPoint point)
	{
		CAudio::Instance()->Stop(BGM_TITLE);
		CAudio::Instance()->Play(BGM_MAIN, true);
		GotoGameState(GAME_STATE_RUN);		// 切換至GAME_STATE_RUN
	}

	void CGameStateInit::OnShow()
	{
		titleImage.ShowBitmap();
		startImage.ShowBitmap();
		tutorialImage.ShowBitmap();
		for (auto it = titleText.begin(); it != titleText.end(); ++it) {
			(*it)->show();
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	// 這個class為遊戲的結束狀態(Game Over)
	/////////////////////////////////////////////////////////////////////////////

	CGameStateOver::CGameStateOver(CGame *g)
		: CGameState(g)
	{

	}

	CGameStateOver::~CGameStateOver() {
		for (auto it = endroll.begin(); it != endroll.end(); ++it)
			delete (*it);
	}

	void CGameStateOver::OnMove()
	{

	}

	void CGameStateOver::OnBeginState()
	{
		winImg.SetTopLeft(640 - (winImg.Width() / 2), 360 - (winImg.Height() / 2));
		loseImg.SetTopLeft(640 - (loseImg.Width() / 2), 360 - (loseImg.Height() / 2));
	}

	void CGameStateOver::OnInit()
	{
		winImg.LoadBitmap("Bitmaps/win.bmp");
		loseImg.LoadBitmap("Bitmaps/lose.bmp");
	}

	void CGameStateOver::OnShow()
	{
		std::string content = "Score: " + to_string(Score::getInstance()->getScore());
		std::string time = "Survive " + to_string(Score::getInstance()->getLifeTime() / 30) + " Seconds";
		std::list<std::string> textList = Score::getInstance()->getTexts();
		if (Score::getInstance()->getWinState() == true) {
			winImg.ShowBitmap();
		}
		else {
			loseImg.ShowBitmap();
		}
		endroll.push_back(new Text(time, Vector2D<int>(0, 30)));
		endroll.push_back(new Text(content, Vector2D<int>(0, 50)));
		endroll.push_back(new Text("Conquer:", Vector2D<int>(0, 70)));
		endroll.push_back(new Text("PRESS ANY KEY", Vector2D<int>(0, 500)));
		endroll.push_back(new Text("OR PRESS ESC TO EXIT", Vector2D<int>(0, 550)));
		for (auto it = endroll.begin(); it != endroll.end(); it++) {
			(*it)->show();
		}
	}

	void CGameStateOver::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
	{
		if (Score::getInstance()->getWinState() == true)
			CAudio::Instance()->Stop(BGM_WIN);
		else
			CAudio::Instance()->Stop(BGM_LOSE);
		const char KEY_ESC = 27;
		const char KEY_SPACE = ' ';
		if (nChar == KEY_SPACE) {
			CAudio::Instance()->Play(BGM_TITLE, true);
			GotoGameState(GAME_STATE_INIT);						// 切換至GAME_STATE_RUN
		}				
		else if (nChar == KEY_ESC) {
			PostMessage(AfxGetMainWnd()->m_hWnd, WM_CLOSE, 0, 0);	// 關閉遊戲
		}
	}

	void CGameStateOver::OnLButtonDown(UINT nFlags, CPoint point)
	{
		if (Score::getInstance()->getWinState() == true)
			CAudio::Instance()->Stop(BGM_WIN);
		else
			CAudio::Instance()->Stop(BGM_LOSE);
		CAudio::Instance()->Play(BGM_TITLE, true);
		GotoGameState(GAME_STATE_INIT);		// 切換至GAME_STATE_RUN
	}

	/////////////////////////////////////////////////////////////////////////////
	// 這個class為遊戲的遊戲執行物件，主要的遊戲程式都在這裡
	/////////////////////////////////////////////////////////////////////////////

	CGameStateRun::CGameStateRun(CGame *g)
		: CGameState(g), mouseSelector(&ships), colliderController(&ships, &gameObjects),
		gameManager(&ships, &gameObjects), testText("testText", Vector2D<int>(550, 600)), shipOperationType(new MoveOperation()), hostileController(&ships, &gameObjects, 300)//, selectedShipStatus("status->", Vector2D<int>(200, 600))
	{
	}

	CGameStateRun::~CGameStateRun()
	{
		for (auto it = borders.begin(); it != borders.end(); ++it)
			delete (*it);
		for (auto it = ships.begin(); it != ships.end(); ++it)
			delete (*it);
		for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it)
			delete (*it);
		delete panel;
		delete shipOperationType;
	}

	void CGameStateRun::OnBeginState()
	{
		gameObjects.clear();
		ships.clear();
		borders.clear();
		OnInit();
	}

	void CGameStateRun::OnMove()							// 移動遊戲元素
	{
		Score::getInstance()->addLifeTime();
		for (auto it = ships.begin(); it != ships.end(); ++it) {
			(*it)->update();
		}
		for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it) {
			(*it)->update();
		}
		for (auto it = borders.begin(); it != borders.end(); ++it) {
			(*it)->update();
		}
		testText.setText("Selected point: " + std::to_string(mouse_position_x) + ", " + std::to_string(mouse_position_y));
		colliderController.update();
		gameManager.update();
		mouseSelector.update(gameMap.getMapCoord());
		panel->getSelectedShips(mouseSelector.getSelectedShips());
		panel->setBorderland(&borders);
		if (gameManager.isWin(borders[1])) {
			Score::getInstance()->setWinState(true);
			CAudio::Instance()->Stop(BGM_MAIN);
			CAudio::Instance()->Play(BGM_WIN, true);
			GotoGameState(GAME_STATE_OVER);
		}
		if (gameManager.isLose(borders[0])) {
			Score::getInstance()->setWinState(false);
			CAudio::Instance()->Stop(BGM_MAIN);
			CAudio::Instance()->Play(BGM_LOSE, true);
			GotoGameState(GAME_STATE_OVER);
		}

		hostileController.searchAndDestroy();
	}


	void CGameStateRun::OnInit()  								// 遊戲的初值及圖形設定
	{
		ShowInitProgress(0);
		panel = new Panel();
		gameMap.LoadBitmap();
		ShowInitProgress(20);
		ships.push_back(new Destroyer("Fubuki", 1000, 200, 100));
		ships.push_back(new Destroyer("Fubuki", 2000, 200, 200));
		ships.push_back(new Destroyer("Fubuki", 2000, 200, 300));
		ships.push_back(new Destroyer("Fubuki", 2000, 300, 100));
		ships.push_back(new Destroyer("Fubuki", 2000, 300, 200));
		ships.push_back(new Battleship("Atago", 1000, 400, 400));
		ships.push_back(new Destroyer("Atago", 2000, 500, 500, false));
		for (auto it = ships.begin(); it != ships.end(); it++) {
			(*it)->setSpeed(2);
		}
		ShowInitProgress(50);
		borders.push_back(new Borderland("Anea", Vector2D<int>(350, 200), Vector2D<int>(800, 400), 500, &ships, MOTHER));
		borders.push_back(new Borderland("Usea", Vector2D<int>(1950, 450), Vector2D<int>(2270, 730), 500, &ships, MOTHER, false));
		borders.push_back(new Borderland("Yuktobania", Vector2D<int>(445, 445), Vector2D<int>(800, 800), 100, &ships, REPAIR));
		borders.push_back(new Borderland("Wellow", Vector2D<int>(1430, 110), Vector2D<int>(1610, 340), 100, &ships, NORMAL, false));
		borders.push_back(new Borderland("North Osea", Vector2D<int>(1120, 300), Vector2D<int>(1450, 540), 100, &ships, NORMAL, false));
		borders.push_back(new Borderland("South Osea", Vector2D<int>(1200, 550), Vector2D<int>(1570, 740), 100, &ships, DEFEND, false));
		borders.push_back(new Borderland("Belka", Vector2D<int>(1550, 440), Vector2D<int>(1800, 670), 100, &ships, REPAIR, false));
		borders.push_back(new Borderland("Sotoa", Vector2D<int>(180, 640), Vector2D<int>(310, 830), 100, &ships, NORMAL));
		borders.push_back(new Borderland("Verusa", Vector2D<int>(450, 820), Vector2D<int>(680, 1070), 100, &ships, DEFEND, false));
		borders.push_back(new Borderland("Aurelia", Vector2D<int>(1200, 810), Vector2D<int>(1440, 1055), 100, &ships, REPAIR, false));
		ShowInitProgress(90);
		hostileController.addPatrolWaypoint(new Vector2D<int>(1000, 1000), new Vector2D<int>(1000, 500));
		ShowInitProgress(100);
	}

	void CGameStateRun::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
	{
		gameMap.onKeyDown(nChar);
		if (nChar == 0x10) {
			isShiftDown = true;
		}

		const char KEY_ESC = 27;
		if (nChar == KEY_ESC) {
			Score::getInstance()->setWinState(false);
			CAudio::Instance()->Stop(BGM_MAIN);
			CAudio::Instance()->Play(BGM_LOSE, true);
			GotoGameState(GAME_STATE_OVER);		}				// 切換至GAME_STATE_RUN
	}

	void CGameStateRun::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
	{
		gameMap.onKeyUp(nChar);
		if (nChar == 0x10) {
			isShiftDown = false;
		}
		else if (nChar == 0x41) {
			shipOperationType = new FireOperation();
		}
	}

	void CGameStateRun::OnLButtonDown(UINT nFlags, CPoint point)  // 處理滑鼠的動作
	{
		mouseSelector.onMouseLDown(point.x, point.y);
	}

	void CGameStateRun::OnLButtonUp(UINT nFlags, CPoint point)	// 處理滑鼠的動作
	{
		mouseSelector.onMouseLUp(point.x, point.y);
	}

	void CGameStateRun::OnMouseMove(UINT nFlags, CPoint point)	// 處理滑鼠的動作
	{
		gameMap.OnMouseMove(point.x, point.y);
	}

	void CGameStateRun::OnRButtonDown(UINT nFlags, CPoint point)  // 處理滑鼠的動作
	{
		mouse_position_x = point.x - gameMap.getMapCoordX();
		mouse_position_y = point.y - gameMap.getMapCoordY();

		MapScreenConvert mapScreenConvert;
		std::list<Ship *> selectedShips = mouseSelector.getSelectedShips();

		shipOperationType->operate(selectedShips, gameObjects, Vector2D<int>(mouse_position_x, mouse_position_y), isShiftDown);
		shipOperationType = new MoveOperation();
	}

	void CGameStateRun::OnRButtonUp(UINT nFlags, CPoint point)	// 處理滑鼠的動作
	{

	}

	void CGameStateRun::OnShow()
	{
		gameMap.OnShow();
		/*may consider conbine show image & show?*/
		for (auto it = borders.begin(); it != borders.end(); ++it) {
			(*it)->showImage(gameMap.getMapCoordX(), gameMap.getMapCoordY());
			if ((*it)->getBorderCd() == 0) {
				srand((unsigned)time(NULL));
				if((*it)->isBelongToFriendly() == false)
					ships.push_back(new Destroyer("X-02A", 120, (*it)->getX(rand() % 300), (*it)->getY(rand() % 300), false));
				else
					ships.push_back(new Destroyer("X-02A", 120, (*it)->getX(rand() % 300), (*it)->getY(rand() % 300), true));
				(*it)->initBorderCd();
			}
			else {
				(*it)->countBorderCd();
			}
		}

		for (auto it = ships.begin(); it != ships.end(); ++it) {
			(*it)->showImage(gameMap.getMapCoordX(), gameMap.getMapCoordY());
		}

		for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it) {
			(*it)->showImage(gameMap.getMapCoordX(), gameMap.getMapCoordY());
		}
		panel->show();
		testText.show();
	}
}
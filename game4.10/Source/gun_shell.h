#ifndef GUN_SHELL_H
#define GUN_SHELL_H

#include "gameObject.h"
#include "vector2D.h"

class GunShell : public GameObject {
public:
	/*Gunshell, looks like watermelon seed*/
	GunShell (int damage, int init_x, int init_y, int dest_x, int dest_y);
	
	// update gunshell position
	void update();

	// countdown before eliminate
	void ttlCountDown();

	// set gunshell flying speed
	void setSpeed(int speed);

	// to check if gunshell do hit something
	void hasCausedDamage();

	// to check if gunshell reach destination
	bool isArrive();

	// to check if gunshell have collider
	bool hasCollider();

	// to get gunshell's damadge
	int getDamage() const;

	// update gunshell's image
	void updateAppearance();
private:
	int ttl = 3;
	int damage;
	int speed = 10;
	Vector2D<int> destination;
};

#endif

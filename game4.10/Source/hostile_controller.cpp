#include "StdAfx.h"
#ifdef _MSC_VER
    #include <ddraw.h>
#endif

#include "hostile_controller.h"
#include "gun_shell.h"

HostileController::HostileController (std::list<Ship *> * ships, std::list<GameObject *> * gameObjects, int searchRange)
    : ships(ships), gameObjects(gameObjects), searchRange(searchRange) {}

HostileController::~HostileController() {

}

void HostileController::setDestination (int x, int y) {
    for (auto it = ships->begin(); it != ships->end(); ++it) {
        (*it)->addDestination(Vector2D<int>(x, y), false);
    }
}

std::list<Ship *> HostileController::getHostileShips () const {
    std::list<Ship *> hostileShips;
    for (auto it = ships->begin(); it != ships->end(); ++it)
        if ((*it)->isFriendly() == false)
            hostileShips.push_back(*it);
    return hostileShips;
}

void HostileController::addPatrolWaypoint(Vector2D<int> * pointA, Vector2D<int> * pointB) {
    wayPointA = pointA;
    wayPointB = pointB;
    setDestination(wayPointA->getX(), wayPointA->getY());
}

void HostileController::patrol () {
    std::list<Ship *> hostileShips = getHostileShips();
    for (auto it = hostileShips.begin(); it != hostileShips.end(); ++it) {
        if (distance(*wayPointA, (*it)->getPosition()) <= 10)
            (*it)->addDestination(*wayPointB, false);
        else if (distance(*wayPointB, (*it)->getPosition()) <= 10)
            (*it)->addDestination(*wayPointA, false);
    }
}

void HostileController::keepInRange (int range) {
    std::list<Ship *> hostileShips = getHostileShips();
	for (auto it = hostileShips.begin(); it != hostileShips.end(); ++it) {
		if (target != nullptr) {
			if (distance((*it)->getPosition(), target->getPosition()) > range) {
				(*it)->addDestination(target->getPosition(), false);
				(*it)->setSpeed(2);
			}
			else {
				(*it)->setSpeed(0);
			}
		}
    }
}

void HostileController::setTarget () {
	for (auto it = ships->begin(); it != ships->end(); ++it) {
        if ((*it)->isFriendly() && distance((*it)->getPosition(), getHostileShips().front()->getPosition()) < searchRange) {
            target = *it;
            break;
        }
    }
}

bool HostileController::search (int range) const {
    std::list<Ship *> hostileShips = getHostileShips();
    for (auto it = hostileShips.begin(); it != hostileShips.end(); ++it) {
        for (auto ship = ships->begin(); ship != ships->end(); ++ship){
            if ((*ship)->isFriendly() && distance((*it)->getPosition(), (*ship)->getPosition()) < range)
                return true;
        }
    }
    return false;
}

void HostileController::destroy() {
    if (target == nullptr)
        return;
    std::list<Ship *> hostileShips = getHostileShips();
    for (auto it = hostileShips.begin(); it != hostileShips.end(); ++it) {
        if ((*it)->isInCd() == false) {
            (*it)->fireShell();
            gameObjects->push_back(new GunShell(100, (*it)->getPosition().getX(), (*it)->getPosition().getY(), target->getPosition().getX(), target->getPosition().getY()));
        }
    }
}
#include <iostream>
void HostileController::searchAndDestroy() {
    if (search(searchRange) == false)
        patrol();
	
    else {
        setTarget();
        keepInRange(100);
        destroy();
    }
}

#include "StdAfx.h"
#ifdef _MSC_VER
	#include <ddraw.h>
	#include "gamelib.h"
#endif

#include <string>
#include "panel.h"
#include "button.h"
#include "text.h"

Panel::Panel() {
	panelImage.LoadBitmap("Bitmaps/panel.bmp");
	panelImage.SetTopLeft(panelCordinate.getX(), panelCordinate.getY());
}

Panel::~Panel() {
	for (list<UI*>::iterator it = this->shipTextList.begin(); it != this->shipTextList.end(); ++it)
		delete *it;
	for (list<UI*>::iterator it = this->borderTextList.begin(); it != this->borderTextList.end(); ++it)
		delete *it;
}

void Panel::getSelectedShips(std::list<Ship *> selectedShips) {
	unsigned counter = 1;
	std::list<UI*> shipTextList;
	for (auto it = selectedShips.begin(); it != selectedShips.end(); ++it) {
		std::string content = "Name: " + (*it)->getName() + "   HitPoint: " + to_string((*it)->hitPoint());
		Text * text = new Text(content, Vector2D<int>(this->panelCordinate.getX() + 120, this->panelCordinate.getY() + (counter * 30) + 20));
		if ((*it)->isFriendly() == false)
			text->setColor(255, 0, 0);
		shipTextList.push_back(text);
		counter++;
	}
	for (list<UI*>::iterator it = this->shipTextList.begin(); it != this->shipTextList.end(); ++it)
		delete *it;
	this->shipTextList = shipTextList;
}

void Panel::show() {
	panelImage.ShowBitmap();
	for (auto it = shipTextList.begin(); it != shipTextList.end(); ++it) {
		(*it)->show();
	}
	for (auto it = borderTextList.begin(); it != borderTextList.end(); ++it) {
		(*it)->show();
	}
}

void Panel::setBorderland(std::vector<Borderland*>* borders) {
	unsigned counter = 1;
	std::list<UI*> borderList;
	for (auto it = borders->begin(); it != borders->end(); it++) {
		if ((*it)->getBaseType() == MOTHER) {
			std::string content = "Border: " + (*it)->getName() + "  Cp: " + (*it)->getHp();
			Text * text = new Text(content, Vector2D<int>(this->panelCordinate.getX() + 900, this->panelCordinate.getY() + (counter * 30) + 20));
			text->setBkColor(0, 0, 0);
			if ((*it)->isBelongToFriendly() == false)
				text->setColor(255, 0, 0);
			else
				text->setColor(0, 255, 255);
			borderList.push_back(text);
			counter++;
		}
	}
	for (list<UI*>::iterator it = this->borderTextList.begin(); it != this->borderTextList.end(); ++it)
		delete *it;
	this->borderTextList = borderList;
}
#ifndef GAMECOMPONENT_H
#define GAMECOMPONENT_H
#include <string>
#include <typeinfo>
class GameComponent {
public:
	// get components' type
     virtual std::string getType(){
         throw (std::string)"Could not be converted to specific component type";
     }
};

#endif

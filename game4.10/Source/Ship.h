#ifndef SHIP_H
#define SHIP_H

#include <string>
#include <queue>
#include "gameObject.h"
#include "transform.h"
#include "vector2D.h"

class Ship : public GameObject{
public:
	/*A parrent class of all ship object*/
    Ship(std::string shipName, unsigned int hp);
    Ship(std::string shipName, unsigned int hitPoint, int positionX, int positionY, bool isFriend);
	virtual ~Ship() = default;
	
	// update performance
	void update();

	// set ship's speed
	void setSpeed(int speed);
	
	// update ships' image
	void updateAppearance();

	// add ships' heading point
    void addDestination(Vector2D<int> dest, bool multiple);
    
	// make ship touchable
	void addCollider();

	// while hit by a gunshell, hurt
    void hit(unsigned damagePoint);

	// feuer
    void fireShell();
    
	// get health status
	unsigned hitPoint() const;
    
	// reloading...
	bool isInCd() const;
    
	// ship sunk if it is sunked
	bool isSunk() const;

	// to recognize enemy or brother
    bool isFriendly() const;

	// to see if reach destination
	void isArrived();

	// repair health
	void hpBuff();

protected:
	std::queue<Vector2D<int>> destination;
    unsigned int hp;
	unsigned int maxHp;
	int speed = 0;
    int fireCd = 0;
	int buffCd = 10;
    bool isFriend = true;
};

#endif

#ifndef BATTLESHIP_H
#define BATTLESHIP_H

#include <string>
#include "Ship.h"

class Battleship : public Ship{
public:
	/*Battleship is a example to show different ship class and differend bitmaps*/
	Battleship(std::string shipName, unsigned int hitPoint);
	Battleship(std::string shipName, unsigned int hitPoint, int positionX, int positionY, bool isFriend = true);
};

#endif

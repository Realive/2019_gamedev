#ifndef ARMAMENT_H
#define ARMAMENT_H

class Armament : public GameObject {
public:
    void update();
    void setSpeed(int speed);
    void hasCausedDamage();
    int getDamage () const;
protected:
    Armament(int damage, int speed, Vector2D<int> destination);
    int ttl = 3;
    int damage;
    int speed;
    Vector2D<int> destination;
};

#endif

#include "StdAfx.h"
#ifdef _MSC_VER
    #include <ddraw.h>
#endif

#include <math.h>
#include "Ship.h"
#include "gameObject.h"
#include "collider.h"
#include "vector2D.h"

Ship::Ship(std::string shipName, unsigned int hitPoint) : GameObject(shipName) {
    hp = hitPoint;
	maxHp = hitPoint;
}

Ship::Ship(std::string shipName, unsigned int hitPoint, int positionX, int positionY, bool isFriend) : GameObject(shipName, positionX, positionY), isFriend(isFriend) {
    hp = hitPoint;
	maxHp = hitPoint;
}

void Ship::update() {
	setPosition(getPosition() + (getDirection() * speed));
	isArrived();
    if (!destination.empty()) {
		setDirection((destination.front() - getPosition()).getUnitVector());
    }
	updateAppearance();
    if (fireCd > 0) {--fireCd;}
}

void Ship::setSpeed(int speed) {
	this->speed = speed;
}

void Ship::updateAppearance() {
	int imageCode = 0;

	if (getDirection().getY() > sin(2 * 3.141 / 16 * 3))
		imageCode = 4;
	else if (getDirection().getY() < sin(2 * 3.141 / 16 * 11))
		imageCode = 0;
	else if (getDirection().getY() < sin(2 * 3.141 / 16 * 3) && getDirection().getY() > sin(2 * 3.141 / 16))
		(getDirection().getX() > 0) ? imageCode = 3 : imageCode = 5;
	else if (getDirection().getY() < sin(2 * 3.141 / 16 * 9) && getDirection().getY() > sin(2 * 3.141 / 16 * 11))
		(getDirection().getX() > 0) ? imageCode = 1 : imageCode = 7;
	else if (getDirection().getY() < sin(2 * 3.141 / 16) && getDirection().getY() > sin(2 * 3.141 / 16 * 9))
		(getDirection().getX() > 0) ? imageCode = 2 : imageCode = 6;

	getComponent<GameImage>()->setImage(imageCode);
}

void Ship::addDestination(Vector2D<int> dest, bool multiple) {
    if (!multiple)
        destination = std::queue<Vector2D<int>> ();
    destination.push(dest);
}

void Ship::addCollider () {
    addComponent(new Collider(getComponent<Transform>(), getComponent<GameImage>()));
}

void Ship::hit(unsigned damagePoint) {
    if (damagePoint > hp)
        hp = 0;
    else
        hp -= damagePoint;
}

void Ship::fireShell () {
    fireCd = 50;
}

unsigned Ship::hitPoint() const {
    return hp;
}

bool Ship::isSunk() const {
    return hp <= 0;
}

bool Ship::isFriendly() const {
    return isFriend;
}

bool Ship::isInCd() const {
    if (fireCd <= 0)
        return false;
    return true;
}

void Ship::isArrived() {
	if (destination.empty()) {
        speed = 0;
		return;
    }
	if ((destination.front() - getPosition()).getLength() < speed) {
		destination.pop();
	}
}

void Ship::hpBuff() {
	if (buffCd == 0) {
		hp++;
		buffCd += 10;
		if (hp >= maxHp)
			hp = maxHp;
	}
	else
		buffCd--;
}

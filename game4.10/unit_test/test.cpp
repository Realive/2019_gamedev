#include "pch.h"
#include "../Source/vector2D.h"

TEST(TestCaseName, TestName) {
  EXPECT_EQ(1, 1);
  EXPECT_TRUE(true);
}


TEST(Vector2D, VectorInit) {
	Vector2D<int> empty_vec;     // 0, 0 as inititial value
	Vector2D<int> init_val_vec(2, 3);

	ASSERT_EQ(0, empty_vec.getX());
	ASSERT_EQ(0, empty_vec.getY());
	ASSERT_EQ(2, init_val_vec.getX());
	ASSERT_EQ(3, init_val_vec.getY());
}
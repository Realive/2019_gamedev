#ifndef MOUSE_SELECTOR_H
#define MOUSE_SELECTOR_H

#include "ship.h"

class MouseSelector {
public:
	MouseSelector(Ship* ships[], size_t shipNumber);
private:
	Ship** ships[];
	size_t shipNumber;
};

#endif